object Form5: TForm5
  Left = 0
  Top = 0
  Caption = 'Form5'
  ClientHeight = 770
  ClientWidth = 900
  Color = clWhite
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = 4539717
  Font.Height = -16
  Font.Name = 'Segoe UI Light'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 21
  object metroSeparator1: TmetroSeparator
    Left = 0
    Top = 74
    Width = 900
    Height = 2
    color = 13487565
    align = alTop
    drawHeight = 1
    drawSide = msTop
    ExplicitTop = 65
  end
  object metroSeparator4: TmetroSeparator
    Left = 378
    Top = 76
    Width = 9
    Height = 694
    color = 30780
    align = alLeft
    drawHeight = 5
    drawSide = msLeft
    ExplicitLeft = 4
    ExplicitTop = 4
    ExplicitHeight = 119
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 1
    Top = 76
    Width = 377
    Height = 693
    Margins.Left = 1
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 1
    Align = alLeft
    BevelOuter = bvNone
    Caption = 'Panel1'
    Color = 15329769
    ParentBackground = False
    ShowCaption = False
    TabOrder = 0
    Visible = False
    DesignSize = (
      377
      693)
    object Label1: TLabel
      Left = 8
      Top = 40
      Width = 123
      Height = 21
      Caption = 'Titre de la t'#226'che'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 5000268
      Font.Height = -16
      Font.Name = 'Segoe UI Light'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 12
      Top = 100
      Width = 75
      Height = 21
      Caption = 'Description'
    end
    object Label4: TLabel
      Left = 8
      Top = 8
      Width = 45
      Height = 21
      Caption = 'Projet'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 5000268
      Font.Height = -16
      Font.Name = 'Segoe UI Light'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 9
      Top = 192
      Width = 88
      Height = 21
      Caption = 'Avancement:'
    end
    object Label6: TLabel
      Left = 205
      Top = 192
      Width = 13
      Height = 21
      Caption = '%'
    end
    object Label8: TLabel
      Left = 24
      Top = 228
      Width = 69
      Height = 21
      Caption = 'Cat'#233'gorie:'
    end
    object Label9: TLabel
      Left = 12
      Top = 473
      Width = 43
      Height = 21
      Anchors = [akLeft, akBottom]
      Caption = 'Label9'
    end
    object metroSeparator3: TmetroSeparator
      Left = 375
      Top = 0
      Width = 2
      Height = 693
      color = 13487565
      align = alRight
      drawHeight = 1
      drawSide = msRight
      ExplicitLeft = 0
      ExplicitHeight = 377
    end
    object Label7: TLabel
      Left = 8
      Top = 257
      Width = 92
      Height = 21
      Caption = 'Pr'#233'd'#233'ceseurs:'
    end
    object CheckBox1: TCheckBox
      Left = 12
      Top = 515
      Width = 129
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'Date de d'#233'but:'
      TabOrder = 0
      OnClick = CheckBox1Click
    end
    object CheckBox2: TCheckBox
      Left = 12
      Top = 551
      Width = 149
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'Date de fin pr'#233'vue:'
      TabOrder = 1
      OnClick = CheckBox2Click
    end
    object DateTimePicker1: TDateTimePicker
      Left = 160
      Top = 507
      Width = 125
      Height = 37
      Anchors = [akLeft, akBottom]
      Date = 41603.500000000000000000
      Time = 41603.500000000000000000
      TabOrder = 2
    end
    object ComboBox2: TComboBox
      Left = 108
      Top = 188
      Width = 93
      Height = 29
      DropDownCount = 11
      TabOrder = 3
      Text = 'ComboBox2'
      OnChange = ComboBox2Change
      Items.Strings = (
        '0'
        '10'
        '20'
        '30'
        '40'
        '50'
        '60'
        '70'
        '80'
        '90'
        '100')
    end
    object DateTimePicker2: TDateTimePicker
      Left = 160
      Top = 547
      Width = 125
      Height = 37
      Anchors = [akLeft, akBottom]
      Date = 41603.500000000000000000
      Time = 41603.500000000000000000
      TabOrder = 4
    end
    object Edit1: TEdit
      Left = 8
      Top = 64
      Width = 361
      Height = 29
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 5
      Text = 'Edit1'
      OnChange = Edit1Change
    end
    object ComboBox1: TComboBox
      Left = 60
      Top = 4
      Width = 309
      Height = 29
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 6
      Text = 'ComboBox1'
      OnChange = Edit1Change
    end
    object Memo1: TMemo
      Left = 8
      Top = 124
      Width = 361
      Height = 57
      Anchors = [akLeft, akTop, akRight]
      Lines.Strings = (
        'Memo1')
      TabOrder = 7
      OnChange = Memo1Change
    end
    object CheckBox3: TCheckBox
      Left = 12
      Top = 591
      Width = 149
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'Date de fin r'#233'elle:'
      TabOrder = 8
      OnClick = CheckBox3Click
    end
    object DateTimePicker3: TDateTimePicker
      Left = 160
      Top = 587
      Width = 125
      Height = 37
      Anchors = [akLeft, akBottom]
      Date = 41603.500000000000000000
      Time = 41603.500000000000000000
      TabOrder = 9
    end
    object ComboBox4: TComboBox
      Left = 108
      Top = 224
      Width = 261
      Height = 29
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 10
      Text = 'ComboBox1'
    end
    object metroButton3: TmetroButton
      Left = 232
      Top = 647
      Width = 136
      Height = 40
      Borders = [mbtop, mbleft, mbbottom, mbright]
      BorderWidth = 1
      BorderRadius = '3,3,3,3'
      ColorBorder = 14277081
      ColorBorderHover = 12105912
      ColorBorderSelected = 14277081
      ColorBorderSelectedHover = 12105912
      ColorBorderDisabled = 14277081
      ColorBorderDisabledHover = 14277081
      ColorBackground = clWhite
      ColorBackgroundHover = 101642
      ColorBackgroundSelected = 101642
      ColorBackgroundSelectedHover = 101642
      ColorBackgroundDisabled = clSilver
      ColorBackgroundDisabledHover = clSilver
      ColorText = 4210752
      ColorTextHover = clWhite
      ColorTextSelected = clWhite
      ColorTextSelectedHover = clWhite
      ColorTextDisabled = 4210752
      ColorTextDisabledHover = 4210752
      ColorShadow = 15987699
      ColorShadowHover = 15987699
      Shadows = '0,0,1,0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -16
      Font.Name = 'segoe ui light'
      Font.Style = []
      Anchors = [akLeft, akBottom]
      Caption = 'Enregistrer'
      TabOrder = 11
      Alignment = taCenter
      Layout = tlCenter
      AlignWithPadding = False
      ShowCaption = True
      TextHoverOnly = False
      OnClick = metroButton3Click
      Selected = False
      Selectable = False
      SelectionGroup = 0
      ButtonType = mbtNormal
      ImageIndex = 0
      DisabledimageIndex = 0
      ImagePos = mbipTop
    end
    object metroLabel2: TmetroLabel
      Left = 160
      Top = 647
      Width = 56
      Height = 41
      Borders = []
      BorderWidth = 1
      BorderRadius = '0,0,0,0'
      ColorBorder = clBlack
      ColorBorderHover = clBlack
      ColorBorderSelected = clBlack
      ColorBorderSelectedHover = clBlack
      ColorBorderDisabled = clBlack
      ColorBorderDisabledHover = clBlack
      ColorBackground = 15329769
      ColorBackgroundHover = clBlack
      ColorBackgroundSelected = clBlack
      ColorBackgroundSelectedHover = clBlack
      ColorBackgroundDisabled = clBlack
      ColorBackgroundDisabledHover = clBlack
      ColorText = 4210752
      ColorTextHover = clBlack
      ColorTextSelected = clBlack
      ColorTextSelectedHover = clBlack
      ColorTextDisabled = clBlack
      ColorTextDisabledHover = clBlack
      ColorShadow = clBlack
      ColorShadowHover = clBlack
      Shadows = '0,0,0,0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -16
      Font.Name = 'segoe ui light'
      Font.Style = []
      IcoSide = msLeft
      IcoPadding = 3
      Hoverable = True
      ImageIndex = 0
      DisabledImageIndex = 0
      Alignment = taLeftJustify
      Anchors = [akLeft, akBottom]
      Caption = 'Annuler'
      Layout = tlCenter
      OnClick = metroLabel2Click
    end
    object ListBox1: TListBox
      Left = 6
      Top = 281
      Width = 319
      Height = 187
      Anchors = [akLeft, akTop, akRight, akBottom]
      ItemHeight = 21
      TabOrder = 13
    end
    object metroButton2: TmetroButton
      Left = 328
      Top = 424
      Width = 45
      Height = 41
      Borders = []
      BorderWidth = 1
      BorderRadius = '3,3,3,3'
      ColorBorder = 14277081
      ColorBorderHover = 12105912
      ColorBorderSelected = 14277081
      ColorBorderSelectedHover = 12105912
      ColorBorderDisabled = 14277081
      ColorBorderDisabledHover = 14277081
      ColorBackground = 15329769
      ColorBackgroundHover = 101642
      ColorBackgroundSelected = 101642
      ColorBackgroundSelectedHover = 101642
      ColorBackgroundDisabled = clSilver
      ColorBackgroundDisabledHover = clSilver
      ColorText = 4210752
      ColorTextHover = clWhite
      ColorTextSelected = clWhite
      ColorTextSelectedHover = clWhite
      ColorTextDisabled = 4210752
      ColorTextDisabledHover = 4210752
      ColorShadow = 15987699
      ColorShadowHover = 15987699
      Shadows = '0,0,1,0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -16
      Font.Name = 'segoe ui light'
      Font.Style = []
      Anchors = [akRight, akBottom]
      Caption = 'metroButton1'
      TabOrder = 14
      Alignment = taCenter
      Layout = tlCenter
      AlignWithPadding = False
      ShowCaption = False
      TextHoverOnly = False
      OnClick = metroButton2Click
      Selected = False
      Selectable = False
      SelectionGroup = 0
      ButtonType = mbtNormal
      Images = Form1.ImageList1
      ImageIndex = 1
      DisabledimageIndex = 0
      ImagePos = mbipTop
    end
  end
  object Panel2: TPanel
    AlignWithMargins = True
    Left = 387
    Top = 76
    Width = 512
    Height = 693
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 1
    Margins.Bottom = 1
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel2'
    ShowCaption = False
    TabOrder = 1
    object metroSeparator2: TmetroSeparator
      Left = 0
      Top = 37
      Width = 512
      Height = 2
      color = 13487565
      align = alTop
      drawHeight = 1
      drawSide = msTop
      ExplicitTop = 65
      ExplicitWidth = 900
    end
    object ScrollBox1: TScrollBox
      Left = 0
      Top = 39
      Width = 512
      Height = 654
      HorzScrollBar.Visible = False
      Align = alClient
      BorderStyle = bsNone
      Color = clWhite
      ParentColor = False
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 512
      Height = 37
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel3'
      Color = clWhite
      ParentBackground = False
      ShowCaption = False
      TabOrder = 1
      object Label3: TLabel
        Left = 8
        Top = 8
        Width = 97
        Height = 21
        Caption = 'Commentaires'
      end
      object metroButton1: TmetroButton
        AlignWithMargins = True
        Left = 465
        Top = 3
        Width = 44
        Height = 31
        Borders = []
        BorderWidth = 1
        BorderRadius = '3,3,3,3'
        ColorBorder = 14277081
        ColorBorderHover = 12105912
        ColorBorderSelected = 14277081
        ColorBorderSelectedHover = 12105912
        ColorBorderDisabled = 14277081
        ColorBorderDisabledHover = 14277081
        ColorBackground = clWhite
        ColorBackgroundHover = 101642
        ColorBackgroundSelected = 101642
        ColorBackgroundSelectedHover = 101642
        ColorBackgroundDisabled = clSilver
        ColorBackgroundDisabledHover = clSilver
        ColorText = 4210752
        ColorTextHover = clWhite
        ColorTextSelected = clWhite
        ColorTextSelectedHover = clWhite
        ColorTextDisabled = 4210752
        ColorTextDisabledHover = 4210752
        ColorShadow = 15987699
        ColorShadowHover = 15987699
        Shadows = '0,0,1,0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'segoe ui light'
        Font.Style = []
        Align = alRight
        Caption = 'Ajouter un commentaire'
        TabOrder = 0
        Alignment = taCenter
        Layout = tlCenter
        AlignWithPadding = False
        ShowCaption = False
        TextHoverOnly = True
        OnClick = metroButton1Click
        Selected = False
        Selectable = False
        SelectionGroup = 0
        ButtonType = mbtNormal
        Images = Form1.ImageList1
        ImageIndex = 1
        DisabledimageIndex = 0
        ImagePos = mbipTop
      end
    end
  end
  object Panel4: TPanel
    AlignWithMargins = True
    Left = 1
    Top = 1
    Width = 898
    Height = 73
    Margins.Left = 1
    Margins.Top = 1
    Margins.Right = 1
    Margins.Bottom = 0
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Panel4'
    Color = clWhite
    ParentBackground = False
    ShowCaption = False
    TabOrder = 2
    DesignSize = (
      898
      73)
    object Label10: TLabel
      Left = 675
      Top = 12
      Width = 88
      Height = 21
      Anchors = [akTop, akRight]
      Caption = 'Avancement:'
      ExplicitLeft = 677
    end
    object Label11: TLabel
      Left = 871
      Top = 12
      Width = 13
      Height = 21
      Anchors = [akTop, akRight]
      Caption = '%'
      ExplicitLeft = 873
    end
    object metroLabel1: TmetroLabel
      Left = 12
      Top = 48
      Width = 65
      Height = 17
      Borders = []
      BorderWidth = 1
      BorderRadius = '0,0,0,0'
      ColorBorder = clBlack
      ColorBorderHover = clBlack
      ColorBorderSelected = clBlack
      ColorBorderSelectedHover = clBlack
      ColorBorderDisabled = clBlack
      ColorBorderDisabledHover = clBlack
      ColorBackground = clWhite
      ColorBackgroundHover = clBlack
      ColorBackgroundSelected = clBlack
      ColorBackgroundSelectedHover = clBlack
      ColorBackgroundDisabled = clBlack
      ColorBackgroundDisabledHover = clBlack
      ColorText = 4210752
      ColorTextHover = clBlack
      ColorTextSelected = clBlack
      ColorTextSelectedHover = clBlack
      ColorTextDisabled = clBlack
      ColorTextDisabledHover = clBlack
      ColorShadow = clBlack
      ColorShadowHover = clBlack
      Shadows = '0,0,0,0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'segoe ui light'
      Font.Style = []
      IcoSide = msLeft
      IcoPadding = 3
      Hoverable = True
      ImageIndex = 0
      DisabledImageIndex = 0
      Alignment = taLeftJustify
      Caption = 'Editer'
      Layout = tlCenter
      OnClick = metroLabel1Click
    end
    object Memo2: TMemo
      Left = 8
      Top = 4
      Width = 635
      Height = 37
      Anchors = [akLeft, akTop, akRight]
      BorderStyle = bsNone
      Color = clWhite
      Enabled = False
      Lines.Strings = (
        'Memo1')
      TabOrder = 0
    end
    object ComboBox3: TComboBox
      Left = 774
      Top = 8
      Width = 93
      Height = 29
      Anchors = [akTop, akRight]
      DropDownCount = 11
      TabOrder = 1
      Text = 'ComboBox2'
      OnChange = ComboBox2Change
      Items.Strings = (
        '0'
        '10'
        '20'
        '30'
        '40'
        '50'
        '60'
        '70'
        '80'
        '90'
        '100')
    end
  end
end
