unit categorieFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TmetrobuttonU, TmetroCustomcontrolU, TmetroLabelU,
  ExtCtrls, TmetroSeparatorU, TmetroFormU, pngimage;

type
  TForm7 = class(TmetroForm)
    Label1: TLabel;
    Edit1: TEdit;
    Panel1: TPanel;
    metroSeparator3: TmetroSeparator;
    Image1: TImage;
    metroLabel1: TmetroLabel;
    metroButton2: TmetroButton;
    procedure Edit1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure metroLabel1Click(Sender: TObject);
    procedure metroButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form7: TForm7;

implementation

{$R *.dfm}

procedure TForm7.Edit1Change(Sender: TObject);
begin
  metrobutton2.Enabled:=edit1.Text<>'';
end;

procedure TForm7.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
  begin
    key:=#0;
    metrobutton2click(sender);
  end;
end;

procedure TForm7.FormCreate(Sender: TObject);
begin
  metroSeparator3.color := $00D4D4D4;
end;

procedure TForm7.FormShow(Sender: TObject);
begin
  edit1.Clear;
end;

procedure TForm7.metroButton2Click(Sender: TObject);
begin
  modalResult:=1;
end;

procedure TForm7.metroLabel1Click(Sender: TObject);
begin
  close;
end;

end.
