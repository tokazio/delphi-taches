unit ajouterPrede;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TmetroCustomcontrolU, TmetrobuttonU, ExtCtrls, StdCtrls, tmetroFormU,
  TmetroLabelU;

type
  TForm10 = class(TmetroForm)
    ListBox1: TListBox;
    Panel1: TPanel;
    metroButton1: TmetroButton;
    metroLabel1: TmetroLabel;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure metroButton1Click(Sender: TObject);
    procedure metroLabel1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    Fpreselected: Tstringlist;
  public
    { Public declarations }
    procedure preselect(its: Tstrings);
  end;

var
  Form10: TForm10;

implementation

uses mainFormU;
{$R *.dfm}

procedure TForm10.FormCreate(Sender: TObject);
begin
  Fpreselected := Tstringlist.Create;
end;

procedure TForm10.preselect(its: Tstrings);
begin
  Fpreselected.assign(its);
end;

procedure TForm10.FormDestroy(Sender: TObject);
begin
  Fpreselected.free;
end;

procedure TForm10.FormShow(Sender: TObject);
begin
  ListBox1.Items.BeginUpdate;
  try
    ListBox1.Clear;
    form1.zquery1.sql.text := 'SELECT * FROM taches WHERE avancement<100 ORDER BY titre,projet';
    form1.zquery1.open;
    form1.zquery1.first;
    while not form1.zquery1.eof do
    begin
      ListBox1.Items.Add(form1.zquery1.FieldByName('titre').AsString);
      if Fpreselected.IndexOf(form1.zquery1.FieldByName('titre').AsString)>=0 then
        Listbox1.selected[listbox1.Items.Count-1]:=true;
      form1.zquery1.next;
    end;
  finally
    ListBox1.Items.EndUpdate;
  end;
end;

procedure TForm10.metroButton1Click(Sender: TObject);
begin
  modalResult := mrok;
end;

procedure TForm10.metroLabel1Click(Sender: TObject);
begin
  close;
end;

end.
