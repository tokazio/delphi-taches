unit nouvelleTacheFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, TmetrobuttonU, TmetroCustomcontrolU,
  TmetroLabelU, ExtCtrls, TmetroSeparatorU, TmetroFormU;

type
  TForm2 = class(TmetroForm)
    Label1: TLabel;
    Edit1: TEdit;
    DateTimePicker1: TDateTimePicker;
    Label2: TLabel;
    Memo1: TMemo;
    Label4: TLabel;
    ComboBox1: TComboBox;
    DateTimePicker2: TDateTimePicker;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    Label5: TLabel;
    ComboBox2: TComboBox;
    Label3: TLabel;
    CheckBox3: TCheckBox;
    DateTimePicker3: TDateTimePicker;
    Label6: TLabel;
    ComboBox3: TComboBox;
    Label7: TLabel;
    Panel1: TPanel;
    metroSeparator3: TmetroSeparator;
    Image1: TImage;
    metroLabel1: TmetroLabel;
    metroButton2: TmetroButton;
    Label8: TLabel;
    ListBox1: TListBox;
    metroButton1: TmetroButton;
    procedure FormShow(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure CheckBox3Click(Sender: TObject);
    procedure metroButton2Click(Sender: TObject);
    procedure metroLabel1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure metroButton1Click(Sender: TObject);
  private
    procedure check;
    procedure listeProjets;
    procedure listeCategories;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses mainFormU, ajouterPrede;
{$R *.dfm}

procedure TForm2.CheckBox1Click(Sender: TObject);
begin
  DateTimePicker1.Enabled := CheckBox1.Checked;
end;

procedure TForm2.CheckBox2Click(Sender: TObject);
begin
  DateTimePicker2.Enabled := CheckBox2.Checked;
end;

procedure TForm2.CheckBox3Click(Sender: TObject);
begin
  DateTimePicker3.Enabled := CheckBox3.Checked;
end;

procedure TForm2.ComboBox1Change(Sender: TObject);
begin
  check;
end;

procedure TForm2.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    Key := #0;
    metroButton2Click(Sender);
  end;
end;

procedure TForm2.check;
begin
  metroButton2.Enabled := (Edit1.text <> '') and (ComboBox1.text <> '');
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  metroSeparator3.color := $00D4D4D4;
end;

procedure TForm2.FormShow(Sender: TObject);
begin
  metroButton2.Enabled := false;
  Memo1.Clear;
  Edit1.Clear;
  DateTimePicker1.DateTime := now();
  DateTimePicker2.DateTime := now();
  DateTimePicker3.DateTime := now();
  ComboBox2.text := '0';
  CheckBox1.Checked := false;
  CheckBox2.Checked := false;
  CheckBox3.Checked := false;
  DateTimePicker1.Enabled := false;
  DateTimePicker2.Enabled := false;
  DateTimePicker3.Enabled := false;
  ListBox1.Clear;
  listeProjets;
  listeCategories;
  Label7.Caption := 'Aujourd''hui le ' + dateToStr(now);
end;

procedure TForm2.listeCategories;
begin
  try
    ComboBox3.Items.BeginUpdate;
    ComboBox3.Clear;
    form1.ZQuery1.SQL.text := 'SELECT titre FROM categories';
    form1.ZQuery1.Open;
    form1.ZQuery1.first;
    while not form1.ZQuery1.Eof do
    begin
      ComboBox3.Items.Add(form1.ZQuery1.FieldByName('titre').AsString);
      form1.ZQuery1.Next;
    end;
  finally
    form1.ZQuery1.close;
    ComboBox3.Items.EndUpdate;
  end;
end;

procedure TForm2.listeProjets;
begin
  try
    ComboBox1.Items.BeginUpdate;
    ComboBox1.Clear;
    form1.ZQuery1.SQL.text := 'SELECT titre FROM projets';
    form1.ZQuery1.Open;
    form1.ZQuery1.first;
    while not form1.ZQuery1.Eof do
    begin
      ComboBox1.Items.Add(form1.ZQuery1.FieldByName('titre').AsString);
      form1.ZQuery1.Next;
    end;
  finally
    form1.ZQuery1.close;
    ComboBox1.Items.EndUpdate;
  end;
  if (form1.projetActif <> '') and (form1.projetActif <> 'Tous') then
    ComboBox1.text := form1.projetActif;
end;

procedure TForm2.metroButton1Click(Sender: TObject);
var
  I: Integer;
begin
  if form10.showmodal = mrok then
  begin
    ListBox1.Clear;
    for I := 0 to form10.ListBox1.Count - 1 do
      if form10.ListBox1.Selected[I] then
        ListBox1.Items.Add(form10.ListBox1.Items[I]);
  end;
end;

procedure TForm2.metroButton2Click(Sender: TObject);
begin
  modalResult := 1;
end;

procedure TForm2.metroLabel1Click(Sender: TObject);
begin
  close;
end;

end.
