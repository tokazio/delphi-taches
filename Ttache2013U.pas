unit Ttache2013U;

interface

uses windows, controls, dialogs, DB, sysutils, generics.collections, classes, dateutils, strutils;

type

  Tmode = (mnone, madd, medit);

  Tligne = class
  Private
    Ffields: Tstringlist;
    function getValue(name: string): string;
  Public
    property values[name: string]: string read getValue; default;
    constructor create(const fs: Tfields; const f: TfieldDefs);
    destructor free;
  end;

function extractFileNameOnly(const filename: string): string;
procedure log(const s: string);
function ApplicationVersion: String;
function humanDate(const date: TDateTime): string;
function humanDateTime(const date: TDateTime): string;
function firstup(const v: string): string;

implementation

function firstup(const v: string): string;
begin
  Result := Uppercase(v[1]) + copy(v, 2, length(v));
end;

function humanDateTime(const date: TDateTime): string;
var
  a: string;
  d, dn, mo, mon, y, yn, h, m, s, ms: word;
  jour, hui: Tdate;
begin
  Result := dateTimeToStr(date);
  decodeDate(now, yn, mon, dn);
  decodeDate(date, y, mo, d);
  jour := encodeDate(y, mo, d);
  hui := encodeDate(yn, mon, dn);
  if jour = hui then
  begin
    DecodeTime(date - now, h, m, s, ms);
    if h = 0 then
    begin
      if m > 0 then
        Result := 'Il y a ' + inttostr(m) + 'min'
      else
        Result := 'Il y a ' + inttostr(s) + 'sec'
    end
    else if h < 5 then
      Result := 'Il y a ' + inttostr(h) + 'h' + inttostr(m)
    else
      Result := 'Aujourd''hui � ' + timeToStr(Ttime(date));
  end
  else if jour = incDay(hui, -1) then
    Result := 'Hier � ' + timeToStr(Ttime(date))
  else if jour = incDay(hui, 1) then
    Result := 'Demain � ' + timeToStr(Ttime(date))
  else if (jour < incDay(hui, 6)) and (jour > hui) then
    Result := firstup(LongDayNames[DayOfWeek(date)]) + ' prochain � ' + timeToStr(Ttime(date))
  else if (jour > incDay(hui, -6)) and (jour < hui) then
    Result := firstup(LongDayNames[DayOfWeek(date)]) + ' dernier � ' + timeToStr(Ttime(date))
end;

function humanDate(const date: TDateTime): string;
var
  a: string;
  d, dn, mo, mon, y, yn, h, m, s, ms: word;
  jour, hui: Tdate;
begin
  Result := dateTimeToStr(date);
  decodeDate(now, yn, mon, dn);
  decodeDate(date, y, mo, d);
  jour := encodeDate(y, mo, d);
  hui := encodeDate(yn, mon, dn);
  if jour = hui then
    Result := 'Aujourd''hui'
  else if jour = incDay(hui, -1) then
    Result := 'Hier'
  else if jour = incDay(hui, 1) then
    Result := 'Demain'
  else if (jour < incDay(hui, 6)) and (jour > hui) then
    Result := firstup(LongDayNames[DayOfWeek(date)]) + ' prochain'
  else if (jour > incDay(hui, -6)) and (jour < hui) then
    Result := firstup(LongDayNames[DayOfWeek(date)]) + ' dernier'
end;

function extractFileNameOnly(const filename: string): string;
begin
  Result := ChangeFileExt(ExtractFileName(filename), '');
end;

procedure log(const s: string);
begin
  OutputDebugString(Pchar(s));
end;

function ApplicationVersion: String;
var
  VerInfoSize, VerValueSize, Dummy: DWord;
  VerInfo: Pointer;
  VerValue: PVSFixedFileInfo;
begin
  VerInfoSize := GetFileVersionInfoSize(Pchar(ParamStr(0)), Dummy);
  { Deux solutions : }
  if VerInfoSize <> 0 then
  { - Les info de version sont inclues }
  begin
    { On alloue de la m�moire pour un pointeur sur les info de version : }
    GetMem(VerInfo, VerInfoSize);
    { On r�cup�re ces informations : }
    GetFileVersionInfo(Pchar(ParamStr(0)), 0, VerInfoSize, VerInfo);
    VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
    { On traite les informations ainsi r�cup�r�es : }
    with VerValue^ do
    begin
      Result := inttostr(dwFileVersionMS shr 16);
      Result := Result + '.' + inttostr(dwFileVersionMS and $FFFF);
      Result := Result + '.' + inttostr(dwFileVersionLS shr 16);
      Result := Result + '.' + inttostr(dwFileVersionLS and $FFFF);
    end;

    { On lib�re la place pr�c�demment allou�e : }
    FreeMem(VerInfo, VerInfoSize);
  end

  else
    { - Les infos de version ne sont pas inclues }
    { On d�clenche une exception dans le programme : }
    raise EAccessViolation.create('Les informations de version de sont pas inclues');
end;

{ Tligne }

constructor Tligne.create(const fs: Tfields; const f: TfieldDefs);
var
  I: Integer;
begin
  Ffields := Tstringlist.create;
  for I := 0 to f.Count - 1 do
    Ffields.Add(f[I].Name + '=' + fs[I].AsString);
end;

destructor Tligne.free;
begin
  Ffields.free;
end;

function Tligne.getValue(name: string): string;
begin
  if Ffields.IndexOfName(name) = -1 then
  begin
    showmessage('Pas de propri�t� ' + name + ' pour la ligne.' + #13 + Ffields.Text);
    Result := '';
  end
  else
    Result := Ffields.values[name];
end;

end.
