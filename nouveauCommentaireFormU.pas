unit nouveauCommentaireFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm4 = class(TForm)
    Label1: TLabel;
    Memo1: TMemo;
    Button2: TButton;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure new;
  end;

var
  Form4: TForm4;

implementation

{$R *.dfm}

procedure TForm4.Button1Click(Sender: TObject);
begin
  modalResult:=1;
end;

procedure TForm4.Button2Click(Sender: TObject);
begin
  close;
end;

procedure Tform4.new;
begin
  memo1.Clear;
  button1.Enabled:=false;
end;

procedure TForm4.Memo1Change(Sender: TObject);
begin
  button1.Enabled:=memo1.Text<>'';
end;

end.
