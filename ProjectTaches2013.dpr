program ProjectTaches2013;

uses
  Forms,
  OExport,
  mainFormU in 'mainFormU.pas' {Form1},
  nouvelleTacheFormU in 'nouvelleTacheFormU.pas' {Form2},
  nouveauProjetFormU in 'nouveauProjetFormU.pas' {Form3},
  nouveauCommentaireFormU in 'nouveauCommentaireFormU.pas' {Form4},
  tacheformU in 'tacheformU.pas' {Form5},
  Ttache2013U in 'Ttache2013U.pas',
  commentaireFormU in 'commentaireFormU.pas' {Form6},
  categorieFormU in 'categorieFormU.pas' {Form7},
  choixprintFormU in 'choixprintFormU.pas' {Form8},
  calFormU in 'calFormU.pas' {Form9},
  ajouterPrede in 'ajouterPrede.pas' {Form10};

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown:=DebugHook<>0;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'T�ches 2013';

  OExportDateFormat:='dd/mm/yyyy';
  OExportTimeFormat:='hh:mm:ss';


  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TForm5, Form5);
  Application.CreateForm(TForm7, Form7);
  Application.CreateForm(TForm8, Form8);
  Application.CreateForm(TForm9, Form9);
  Application.CreateForm(TForm10, Form10);
  Application.Run;
end.
