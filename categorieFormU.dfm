object Form7: TForm7
  Left = 0
  Top = 0
  Caption = 'Nouvelle cat'#233'gorie'
  ClientHeight = 219
  ClientWidth = 570
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clGray
  Font.Height = -16
  Font.Name = 'Segoe UI Light'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    570
    219)
  PixelsPerInch = 96
  TextHeight = 21
  object Label1: TLabel
    Left = 232
    Top = 36
    Width = 159
    Height = 21
    Caption = 'Nom de la cat'#233'gorie:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 5000268
    Font.Height = -16
    Font.Name = 'Segoe UI Light'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 228
    Top = 64
    Width = 333
    Height = 29
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Text = 'Edit1'
    OnChange = Edit1Change
    OnKeyPress = Edit1KeyPress
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 1
    Top = 1
    Width = 221
    Height = 217
    Margins.Left = 1
    Margins.Top = 1
    Margins.Right = 0
    Margins.Bottom = 1
    Align = alLeft
    BevelOuter = bvNone
    Caption = 'Panel1'
    Color = 15329769
    ParentBackground = False
    ShowCaption = False
    TabOrder = 1
    object metroSeparator3: TmetroSeparator
      Left = 219
      Top = 0
      Width = 2
      Height = 217
      color = 13487565
      align = alRight
      drawHeight = 1
      drawSide = msRight
      ExplicitLeft = 0
      ExplicitHeight = 377
    end
    object Image1: TImage
      Left = 16
      Top = 32
      Width = 185
      Height = 169
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000300000
        003008060000005702F987000000017352474200AECE1CE90000000467414D41
        0000B18F0BFC6105000000097048597300000EC300000EC301C76FA864000000
        A24944415478DAEDD3DD0D4030188561DDCC548660299B610189C6F9E9C73989
        BB6ADEE7A26D2ABEE60E08C01D10803B200077C02880F9C5BFFB0880C3D510C0
        D701ADF34C00013CBCB3F77C00BF02DCDD5FE60D04301280DE104000C501DBF5
        2D5501AFE39D0048BC0B008B7700A0F16A003C5E09A0C4AB00B47805801ACF06
        D0E39900493C0B208B6700A4F168803C1E09581DF148806D01B817807B01B857
        1E700259193731DA439A3D0000000049454E44AE426082}
    end
  end
  object metroLabel1: TmetroLabel
    Left = 380
    Top = 171
    Width = 62
    Height = 40
    Borders = []
    BorderWidth = 1
    BorderRadius = '0,0,0,0'
    ColorBorder = clBlack
    ColorBorderHover = clBlack
    ColorBorderSelected = clBlack
    ColorBorderSelectedHover = clBlack
    ColorBorderDisabled = clBlack
    ColorBorderDisabledHover = clBlack
    ColorBackground = clWhite
    ColorBackgroundHover = clBlack
    ColorBackgroundSelected = clBlack
    ColorBackgroundSelectedHover = clBlack
    ColorBackgroundDisabled = clBlack
    ColorBackgroundDisabledHover = clBlack
    ColorText = 4210752
    ColorTextHover = clBlack
    ColorTextSelected = clBlack
    ColorTextSelectedHover = clBlack
    ColorTextDisabled = clBlack
    ColorTextDisabledHover = clBlack
    ColorShadow = clBlack
    ColorShadowHover = clBlack
    Shadows = '0,0,0,0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'segoe ui light'
    Font.Style = []
    IcoSide = msLeft
    IcoPadding = 3
    Hoverable = True
    ImageIndex = 0
    DisabledImageIndex = 0
    Alignment = taLeftJustify
    Anchors = [akRight, akBottom]
    Caption = 'Annuler'
    Layout = tlCenter
    OnClick = metroLabel1Click
  end
  object metroButton2: TmetroButton
    Left = 462
    Top = 170
    Width = 100
    Height = 41
    Borders = [mbtop, mbleft, mbbottom, mbright]
    BorderWidth = 1
    BorderRadius = '3,3,3,3'
    ColorBorder = 14277081
    ColorBorderHover = 12105912
    ColorBorderSelected = 14277081
    ColorBorderSelectedHover = 12105912
    ColorBorderDisabled = 14277081
    ColorBorderDisabledHover = 14277081
    ColorBackground = clWhite
    ColorBackgroundHover = 101642
    ColorBackgroundSelected = 101642
    ColorBackgroundSelectedHover = 101642
    ColorBackgroundDisabled = clSilver
    ColorBackgroundDisabledHover = clSilver
    ColorText = 4210752
    ColorTextHover = clWhite
    ColorTextSelected = clWhite
    ColorTextSelectedHover = clWhite
    ColorTextDisabled = 4210752
    ColorTextDisabledHover = 4210752
    ColorShadow = 15987699
    ColorShadowHover = 15987699
    Shadows = '0,0,1,0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'segoe ui light'
    Font.Style = []
    Anchors = [akRight, akBottom]
    Caption = 'Cr'#233'er'
    TabOrder = 3
    Alignment = taCenter
    Layout = tlCenter
    AlignWithPadding = False
    ShowCaption = True
    TextHoverOnly = False
    OnClick = metroButton2Click
    Selected = False
    Selectable = False
    SelectionGroup = 0
    ButtonType = mbtNormal
    ImageIndex = 0
    DisabledimageIndex = 0
    ImagePos = mbipTop
  end
end
