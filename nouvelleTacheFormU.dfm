object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Nouvelle t'#226'che'
  ClientHeight = 548
  ClientWidth = 803
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clGray
  Font.Height = -16
  Font.Name = 'Segoe UI Ligth'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    803
    548)
  PixelsPerInch = 96
  TextHeight = 18
  object Label1: TLabel
    Left = 232
    Top = 68
    Width = 128
    Height = 21
    Caption = 'Titre de la t'#226'che:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 5000268
    Font.Height = -16
    Font.Name = 'Segoe UI Light'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 236
    Top = 128
    Width = 84
    Height = 18
    Caption = 'Description:'
  end
  object Label4: TLabel
    Left = 232
    Top = 36
    Width = 50
    Height = 21
    Caption = 'Projet:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 5000268
    Font.Height = -16
    Font.Name = 'Segoe UI Light'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 610
    Top = 96
    Width = 90
    Height = 18
    Anchors = [akTop, akRight]
    Caption = 'Avancement:'
    Visible = False
    ExplicitLeft = 636
  end
  object Label3: TLabel
    Left = 782
    Top = 96
    Width = 14
    Height = 18
    Anchors = [akTop, akRight]
    Caption = '%'
    Visible = False
    ExplicitLeft = 808
  end
  object Label6: TLabel
    Left = 542
    Top = 36
    Width = 74
    Height = 18
    Anchors = [akTop, akRight]
    Caption = 'Cat'#233'gorie:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'Segoe UI Ligth'
    Font.Style = []
    ParentFont = False
    ExplicitLeft = 568
  end
  object Label7: TLabel
    Left = 240
    Top = 521
    Width = 63
    Height = 16
    Anchors = [akLeft, akBottom]
    Caption = 'Aujourd'#39'hui'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -13
    Font.Name = 'Segoe UI Ligth'
    Font.Style = []
    ParentFont = False
    ExplicitTop = 506
  end
  object Label8: TLabel
    Left = 236
    Top = 305
    Width = 102
    Height = 18
    Anchors = [akLeft, akBottom]
    Caption = 'Pr'#233'd'#233'ceseurs:'
    ExplicitTop = 292
  end
  object Edit1: TEdit
    Left = 232
    Top = 92
    Width = 347
    Height = 26
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Text = 'Edit1'
    OnChange = ComboBox1Change
    OnKeyPress = Edit1KeyPress
  end
  object DateTimePicker1: TDateTimePicker
    Left = 365
    Top = 449
    Width = 125
    Height = 26
    Anchors = [akLeft, akBottom]
    Date = 41603.394480023150000000
    Time = 41603.394480023150000000
    TabOrder = 1
  end
  object Memo1: TMemo
    Left = 232
    Top = 148
    Width = 558
    Height = 152
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'Memo1')
    TabOrder = 2
  end
  object ComboBox1: TComboBox
    Left = 288
    Top = 32
    Width = 199
    Height = 26
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 3
    Text = 'ComboBox1'
    OnChange = ComboBox1Change
  end
  object DateTimePicker2: TDateTimePicker
    Left = 661
    Top = 449
    Width = 125
    Height = 26
    Anchors = [akLeft, akBottom]
    Date = 41603.394480023150000000
    Time = 41603.394480023150000000
    TabOrder = 4
  end
  object CheckBox1: TCheckBox
    Left = 236
    Top = 453
    Width = 129
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'Date de d'#233'but:'
    TabOrder = 5
    OnClick = CheckBox1Click
  end
  object CheckBox2: TCheckBox
    Left = 512
    Top = 453
    Width = 149
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'Date defin pr'#233'vue:'
    TabOrder = 6
    OnClick = CheckBox2Click
  end
  object ComboBox2: TComboBox
    Left = 707
    Top = 92
    Width = 68
    Height = 26
    Anchors = [akTop, akRight]
    DropDownCount = 11
    TabOrder = 7
    Text = 'ComboBox2'
    Visible = False
    Items.Strings = (
      '0'
      '10'
      '20'
      '30'
      '40'
      '50'
      '60'
      '70'
      '80'
      '90'
      '100')
  end
  object CheckBox3: TCheckBox
    Left = 236
    Top = 490
    Width = 149
    Height = 15
    Anchors = [akLeft, akBottom]
    Caption = 'Date de fin r'#233'elle:'
    TabOrder = 8
    Visible = False
    OnClick = CheckBox3Click
  end
  object DateTimePicker3: TDateTimePicker
    Left = 384
    Top = 486
    Width = 125
    Height = 24
    Anchors = [akLeft, akBottom]
    Date = 41603.500000000000000000
    Time = 41603.500000000000000000
    TabOrder = 9
    Visible = False
  end
  object ComboBox3: TComboBox
    Left = 626
    Top = 32
    Width = 169
    Height = 26
    Anchors = [akTop, akRight]
    TabOrder = 10
    Text = 'ComboBox1'
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 1
    Top = 1
    Width = 221
    Height = 546
    Margins.Left = 1
    Margins.Top = 1
    Margins.Right = 0
    Margins.Bottom = 1
    Align = alLeft
    BevelOuter = bvNone
    Caption = 'Panel1'
    Color = 15329769
    ParentBackground = False
    ShowCaption = False
    TabOrder = 11
    object metroSeparator3: TmetroSeparator
      Left = 219
      Top = 0
      Width = 2
      Height = 546
      color = 13487565
      align = alRight
      drawHeight = 1
      drawSide = msRight
      ExplicitLeft = 0
      ExplicitHeight = 377
    end
    object Image1: TImage
      Left = 16
      Top = 40
      Width = 185
      Height = 181
    end
  end
  object metroLabel1: TmetroLabel
    Left = 622
    Top = 500
    Width = 53
    Height = 40
    Borders = []
    BorderWidth = 1
    BorderRadius = '0,0,0,0'
    ColorBorder = clBlack
    ColorBorderHover = clBlack
    ColorBorderSelected = clBlack
    ColorBorderSelectedHover = clBlack
    ColorBorderDisabled = clBlack
    ColorBorderDisabledHover = clBlack
    ColorBackground = clWhite
    ColorBackgroundHover = clBlack
    ColorBackgroundSelected = clBlack
    ColorBackgroundSelectedHover = clBlack
    ColorBackgroundDisabled = clBlack
    ColorBackgroundDisabledHover = clBlack
    ColorText = 4210752
    ColorTextHover = clBlack
    ColorTextSelected = clBlack
    ColorTextSelectedHover = clBlack
    ColorTextDisabled = clBlack
    ColorTextDisabledHover = clBlack
    ColorShadow = clBlack
    ColorShadowHover = clBlack
    Shadows = '0,0,0,0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'segoe ui light'
    Font.Style = []
    IcoSide = msLeft
    IcoPadding = 3
    Hoverable = True
    ImageIndex = 0
    DisabledImageIndex = 0
    Alignment = taLeftJustify
    Anchors = [akRight, akBottom]
    Caption = 'Annuler'
    Layout = tlCenter
    OnClick = metroLabel1Click
  end
  object metroButton2: TmetroButton
    Left = 691
    Top = 499
    Width = 100
    Height = 41
    Borders = [mbtop, mbleft, mbbottom, mbright]
    BorderWidth = 1
    BorderRadius = '3,3,3,3'
    ColorBorder = 14277081
    ColorBorderHover = 12105912
    ColorBorderSelected = 14277081
    ColorBorderSelectedHover = 12105912
    ColorBorderDisabled = 14277081
    ColorBorderDisabledHover = 14277081
    ColorBackground = clWhite
    ColorBackgroundHover = 101642
    ColorBackgroundSelected = 101642
    ColorBackgroundSelectedHover = 101642
    ColorBackgroundDisabled = clSilver
    ColorBackgroundDisabledHover = clSilver
    ColorText = 4210752
    ColorTextHover = clWhite
    ColorTextSelected = clWhite
    ColorTextSelectedHover = clWhite
    ColorTextDisabled = 4210752
    ColorTextDisabledHover = 4210752
    ColorShadow = 15987699
    ColorShadowHover = 15987699
    Shadows = '0,0,1,0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'segoe ui light'
    Font.Style = []
    Anchors = [akRight, akBottom]
    Caption = 'Cr'#233'er'
    TabOrder = 13
    Alignment = taCenter
    Layout = tlCenter
    AlignWithPadding = False
    ShowCaption = True
    TextHoverOnly = False
    OnClick = metroButton2Click
    Selected = False
    Selectable = False
    SelectionGroup = 0
    ButtonType = mbtNormal
    ImageIndex = 0
    DisabledimageIndex = 0
    ImagePos = mbipTop
  end
  object ListBox1: TListBox
    Left = 232
    Top = 329
    Width = 499
    Height = 108
    Anchors = [akLeft, akRight, akBottom]
    ItemHeight = 18
    TabOrder = 14
  end
  object metroButton1: TmetroButton
    Left = 742
    Top = 393
    Width = 45
    Height = 41
    Borders = []
    BorderWidth = 1
    BorderRadius = '3,3,3,3'
    ColorBorder = 14277081
    ColorBorderHover = 12105912
    ColorBorderSelected = 14277081
    ColorBorderSelectedHover = 12105912
    ColorBorderDisabled = 14277081
    ColorBorderDisabledHover = 14277081
    ColorBackground = clWhite
    ColorBackgroundHover = 101642
    ColorBackgroundSelected = 101642
    ColorBackgroundSelectedHover = 101642
    ColorBackgroundDisabled = clSilver
    ColorBackgroundDisabledHover = clSilver
    ColorText = 4210752
    ColorTextHover = clWhite
    ColorTextSelected = clWhite
    ColorTextSelectedHover = clWhite
    ColorTextDisabled = 4210752
    ColorTextDisabledHover = 4210752
    ColorShadow = 15987699
    ColorShadowHover = 15987699
    Shadows = '0,0,1,0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'segoe ui light'
    Font.Style = []
    Anchors = [akRight, akBottom]
    Caption = 'metroButton1'
    TabOrder = 15
    Alignment = taCenter
    Layout = tlCenter
    AlignWithPadding = False
    ShowCaption = False
    TextHoverOnly = False
    OnClick = metroButton1Click
    Selected = False
    Selectable = False
    SelectionGroup = 0
    ButtonType = mbtNormal
    Images = Form1.ImageList1
    ImageIndex = 1
    DisabledimageIndex = 0
    ImagePos = mbipTop
  end
end
