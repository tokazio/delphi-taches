unit choixprintFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TmetrobuttonU, TmetroCustomcontrolU, TmetrocheckboxU, ComCtrls, dateutils;

type
  TForm8 = class(TForm)
    metroCheckBox1: TmetroCheckBox;
    metroCheckBox2: TmetroCheckBox;
    metroButton1: TmetroButton;
    DateTimePicker1: TDateTimePicker;
    metroCheckBox3: TmetroCheckBox;
    procedure metroButton1Click(Sender: TObject);
    procedure metroCheckBox3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure metroCheckBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form8: TForm8;

implementation

{$R *.dfm}

procedure TForm8.FormShow(Sender: TObject);
begin
  dateTimePicker1.DateTime:=incDay(now,-30);
end;

procedure TForm8.metroButton1Click(Sender: TObject);
begin
  modalResult:=1;
end;

procedure TForm8.metroCheckBox1Click(Sender: TObject);
begin
  metrobutton1.enabled:=metrocheckbox1.Checked or metrocheckbox2.Checked;

end;

procedure TForm8.metroCheckBox3Click(Sender: TObject);
begin
  dateTimePicker1.Enabled:=metroCheckbox3.Checked;
end;

end.
