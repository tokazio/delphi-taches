unit calFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TmetroFormU, ExtCtrls, TmetroSeparatorU, StdCtrls, dateUtils,
  TmetroCustomcontrolU, TmetrobuttonU;

type
  Tcube = class
  Private
    Fr: Trect;
    Ftitre: String;
  published
    property r: Trect read Fr;
    property titre: String read Ftitre;
  Public
    constructor create(const r: Trect; const titre: string);
  end;

  TForm9 = class(TmetroForm)
    PaintBox1: TPaintBox;
    Panel1: TPanel;
    metroSeparator1: TmetroSeparator;
    Panel2: TPanel;
    Label2: TLabel;
    metroButton4: TmetroButton;
    metroButton1: TmetroButton;
    Label1: TLabel;
    metroButton2: TmetroButton;
    metroButton3: TmetroButton;
    procedure FormCreate(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PaintBox1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure PaintBox1DblClick(Sender: TObject);
    procedure metroButton2Click(Sender: TObject);
    procedure metroButton3Click(Sender: TObject);
  private
    { Private declarations }
    dateAff: Tdatetime;
    cubes: Tlist;
    xclic, yclic: Integer;
    procedure clearCubes;
  public
    { Public declarations }
    procedure draw(canvas: Tcanvas; header: boolean = false);
  end;

var
  Form9: TForm9;

implementation

uses mainFormU, tacheformU;
{$R *.dfm}

procedure TForm9.FormCreate(Sender: TObject);
begin
  metroSeparator1.color := $00D4D4D4;
  dateAff := now;
  cubes := Tlist.create;
end;

procedure TForm9.FormDestroy(Sender: TObject);
begin
  clearCubes;
  cubes.Free;
end;

procedure TForm9.metroButton2Click(Sender: TObject);
begin
  dateAff := incMonth(dateAff, -1);
  PaintBox1.Repaint;
end;

procedure TForm9.metroButton3Click(Sender: TObject);
begin
  dateAff := incMonth(dateAff, 1);
  PaintBox1.Repaint;
end;

procedure TForm9.clearCubes;
var
  I: Integer;
begin
  for I := 0 to cubes.Count - 1 do
  begin
    Tcube(cubes[I]).Free;
    cubes[I] := nil;
  end;
  cubes.Clear;
end;

procedure TForm9.PaintBox1DblClick(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to cubes.Count - 1 do
    if ptInrect(Tcube(cubes[I]).r, point(xclic, yclic)) then
    begin
      form5.load(Tcube(cubes[I]).titre); ;
      break;
    end;

end;

procedure TForm9.PaintBox1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  xclic := X;
  yclic := Y;
end;

procedure TForm9.PaintBox1Paint(Sender: TObject);
begin
  clearCubes;
  draw(PaintBox1.canvas);
end;

procedure TForm9.draw(canvas: Tcanvas; header: boolean = false);
var
  w, h, ht: Integer;
  I, j, k, l: Integer;
  t: string;
  myY, nY, myM, nM, myD, nD: word;
  r: Trect;
  HHEAD, HTITRE: Integer;
begin
  // affichage mois
  with canvas do
  begin
    font.Name := 'Arial';
    font.Size := 10;
    ht := textHeight('Aq"');
    decodeDate(dateAff, myY, myM, myD);
    decodeDate(now, nY, nM, nD);
    Label2.Caption := LongMonthNames[myM] + ' ' + inttostr(myY);
    HTITRE := 0;
    if header then
    begin
      font.Name := 'Segoe ui light';
      font.Size := 22;
      textout(20, 20, 'T�ches de ' + Label2.Caption + ' au ' + formatDateTime('dd/mm/yyyy', now));
      HTITRE := 20 + textHeight(Label2.Caption) + 10;
      font.Size := 10;
      font.Name := 'Arial';
    end;
    HHEAD := textHeight('V') + 10;
    w := round(cliprect.right / 5);
    // en t�te jours
    font.color := clwhite;
    for I := 0 to 4 do
    begin
      if (myY = nY) and (myM = nM) and (I = DayOfWeek(now) - 2) then
        brush.color := $00B55B
      else
        brush.color := $00783C;
      pen.color := brush.color;
      rectangle(I * w - 1, HTITRE, I * w + w, HTITRE + HHEAD);
      textrect(rect(I * w + 1, HTITRE + 1, I * w + w - 1, HTITRE + HHEAD - 1), I * w + 1 + HHEAD, HTITRE + round((HHEAD - ht) / 2), upperCase(LongDayNames[I + 2]));
    end;
    ht := ht + 4;
    h := round((cliprect.bottom - HHEAD - HTITRE) / 5);
    brush.Style := bsclear;
    font.Name := 'Tahoma';
    k := -DayOfWeek(encodeDate(myY, myM, 1)) + 2 + 1;
    if k <= -4 then
      k := k + 7;
    for I := 0 to 4 do
    begin
      for j := 0 to 4 do
      begin
        pen.color := $00D4D4D4;
        if (myY = nY) and (myM = nM) and (k = nD) then
        begin
          brush.Style := bssolid;
          brush.color := $00783C;
        end
        else
        begin
          brush.Style := bsclear;
        end;
        rectangle(j * w - 1, I * h + HTITRE + HHEAD - 1, j * w + w, I * h + h + HTITRE + HHEAD);
        if k < 1 then
        begin

        end
        else if k > DaysInAMonth(myY, myM) then
        begin

        end
        else
        begin
          if (myY = nY) and (myM = nM) and (k = nD) then
            font.color := clwhite
          else
            font.color := clgray;
          textout(j * w + 5, I * h + HTITRE + HHEAD + 5, inttostr(k));
          font.color := clwhite;
          form1.zquery1.sql.text := 'SELECT titre,(datetermine is not null AND datetermine<=datefin) as finiok,(datetermine is not null AND datetermine>=datefin) as finino,(datefin>NOW()) as futur FROM taches WHERE datefin=''' + formatDateTime('dd/MM/yyyy', encodeDate(myY, myM, k)) + '''';
          form1.zquery1.Open;
          form1.zquery1.First;
          l := 0;
          while not form1.zquery1.Eof do
          begin
            r := rect(j * w + 5, I * h + HTITRE + HHEAD + ht + 5 + l * ht + 2, j * w + w - 8, I * h + ht + HTITRE + HHEAD + 5 + l * ht + ht);
            brush.Style := bssolid;
            font.color := clwhite;
            // si fini dans les temps, fond vert texte blanc
            if form1.zquery1.FieldByName('finiok').AsBoolean then
              brush.color := $76FFC1
              // si � finir, fond bleu texte blanc
            else if form1.zquery1.FieldByName('futur').AsBoolean then
              brush.color := $FFC176
              // si fini en retard, fond vert texte rouge
            else if form1.zquery1.FieldByName('finino').AsBoolean then
            begin
              brush.color := $76FFC1;
              font.color := clred;
            end
            else
              // si pas fini et en retard, fond rouge
              brush.color := $769EFF;
            pen.color := brush.color;
            roundrect(r.Left, r.Top, r.right, r.bottom, 4, 4);
            brush.Style := bsclear;
            inflaterect(r, -1, -1);
            if I * h + HTITRE + HHEAD + ht + 5 + (l + 1) * ht + 2 + ht > (I + 1) * h + HTITRE + HHEAD then
            begin
              textout(r.Left + 2, r.Top, '...');
              break;
            end
            else
            begin
              cubes.add(Tcube.create(r, form1.zquery1.FieldByName('titre').AsString));
              textrect(r, r.Left + 2, r.Top, form1.zquery1.FieldByName('titre').AsString);
            end;
            form1.zquery1.Next;
            inc(l);
          end;
        end;
        inc(k);
      end;
      inc(k, 2);
    end;
  end;
end;

{ Tcube }

constructor Tcube.create(const r: Trect; const titre: string);
begin
  Fr := r;
  Ftitre := titre;
end;

end.
