object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Nouveau projet'
  ClientHeight = 308
  ClientWidth = 893
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clGray
  Font.Height = -16
  Font.Name = 'Segoe UI Light'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    893
    308)
  PixelsPerInch = 96
  TextHeight = 21
  object Label1: TLabel
    Left = 232
    Top = 36
    Width = 116
    Height = 21
    Caption = 'Nom du projet:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 5000268
    Font.Height = -16
    Font.Name = 'Segoe UI Light'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 232
    Top = 72
    Width = 75
    Height = 21
    Caption = 'Description'
  end
  object Label3: TLabel
    Left = 653
    Top = 36
    Width = 69
    Height = 21
    Anchors = [akTop, akRight]
    Caption = 'Cat'#233'gorie:'
  end
  object Label4: TLabel
    Left = 230
    Top = 200
    Width = 63
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'Aujourd'#39'hui'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -13
    Font.Name = 'Segoe UI Light'
    Font.Style = []
    ParentFont = False
    ExplicitTop = 330
  end
  object Edit1: TEdit
    Left = 356
    Top = 32
    Width = 228
    Height = 29
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    Text = 'Edit1'
    OnChange = Edit1Change
    OnKeyPress = Edit1KeyPress
  end
  object Memo1: TMemo
    Left = 228
    Top = 96
    Width = 658
    Height = 93
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
  end
  object DateTimePicker1: TDateTimePicker
    Left = 372
    Top = 220
    Width = 117
    Height = 29
    Anchors = [akLeft, akBottom]
    Date = 41603.394480023150000000
    Time = 41603.394480023150000000
    TabOrder = 2
  end
  object DateTimePicker2: TDateTimePicker
    Left = 660
    Top = 220
    Width = 117
    Height = 29
    Anchors = [akLeft, akBottom]
    Date = 41603.394480023150000000
    Time = 41603.394480023150000000
    TabOrder = 3
  end
  object CheckBox1: TCheckBox
    Left = 233
    Top = 228
    Width = 129
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'Date de d'#233'but:'
    TabOrder = 4
    OnClick = CheckBox1Click
  end
  object CheckBox2: TCheckBox
    Left = 505
    Top = 228
    Width = 149
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'Date defin pr'#233'vue:'
    TabOrder = 5
    OnClick = CheckBox2Click
  end
  object ComboBox1: TComboBox
    Left = 737
    Top = 32
    Width = 145
    Height = 29
    Anchors = [akTop, akRight]
    TabOrder = 6
    Text = 'ComboBox1'
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 1
    Top = 1
    Width = 221
    Height = 306
    Margins.Left = 1
    Margins.Top = 1
    Margins.Right = 0
    Margins.Bottom = 1
    Align = alLeft
    BevelOuter = bvNone
    Caption = 'Panel1'
    Color = 15329769
    ParentBackground = False
    ShowCaption = False
    TabOrder = 7
    object metroSeparator3: TmetroSeparator
      Left = 219
      Top = 0
      Width = 2
      Height = 306
      color = 13487565
      align = alRight
      drawHeight = 1
      drawSide = msRight
      ExplicitLeft = 0
      ExplicitHeight = 377
    end
    object Image1: TImage
      Left = 16
      Top = 32
      Width = 185
      Height = 181
    end
  end
  object metroButton2: TmetroButton
    Left = 785
    Top = 260
    Width = 100
    Height = 41
    Borders = [mbtop, mbleft, mbbottom, mbright]
    BorderWidth = 1
    BorderRadius = '3,3,3,3'
    ColorBorder = 14277081
    ColorBorderHover = 12105912
    ColorBorderSelected = 14277081
    ColorBorderSelectedHover = 12105912
    ColorBorderDisabled = 14277081
    ColorBorderDisabledHover = 14277081
    ColorBackground = clWhite
    ColorBackgroundHover = 101642
    ColorBackgroundSelected = 101642
    ColorBackgroundSelectedHover = 101642
    ColorBackgroundDisabled = clSilver
    ColorBackgroundDisabledHover = clSilver
    ColorText = 4210752
    ColorTextHover = clWhite
    ColorTextSelected = clWhite
    ColorTextSelectedHover = clWhite
    ColorTextDisabled = 4210752
    ColorTextDisabledHover = 4210752
    ColorShadow = 15987699
    ColorShadowHover = 15987699
    Shadows = '0,0,1,0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'segoe ui light'
    Font.Style = []
    Anchors = [akRight, akBottom]
    Caption = 'Enregistrer'
    TabOrder = 8
    Alignment = taCenter
    Layout = tlCenter
    AlignWithPadding = False
    ShowCaption = True
    TextHoverOnly = False
    OnClick = metroButton2Click
    Selected = False
    Selectable = False
    SelectionGroup = 0
    ButtonType = mbtNormal
    ImageIndex = 0
    DisabledimageIndex = 0
    ImagePos = mbipTop
  end
  object metroLabel1: TmetroLabel
    Left = 696
    Top = 262
    Width = 73
    Height = 40
    Borders = []
    BorderWidth = 1
    BorderRadius = '0,0,0,0'
    ColorBorder = clBlack
    ColorBorderHover = clBlack
    ColorBorderSelected = clBlack
    ColorBorderSelectedHover = clBlack
    ColorBorderDisabled = clBlack
    ColorBorderDisabledHover = clBlack
    ColorBackground = clWhite
    ColorBackgroundHover = clBlack
    ColorBackgroundSelected = clBlack
    ColorBackgroundSelectedHover = clBlack
    ColorBackgroundDisabled = clBlack
    ColorBackgroundDisabledHover = clBlack
    ColorText = 4210752
    ColorTextHover = clBlack
    ColorTextSelected = clBlack
    ColorTextSelectedHover = clBlack
    ColorTextDisabled = clBlack
    ColorTextDisabledHover = clBlack
    ColorShadow = clBlack
    ColorShadowHover = clBlack
    Shadows = '0,0,0,0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'segoe ui light'
    Font.Style = []
    IcoSide = msLeft
    IcoPadding = 3
    Hoverable = True
    ImageIndex = 0
    DisabledImageIndex = 0
    Alignment = taLeftJustify
    Anchors = [akRight, akBottom]
    Caption = 'Annuler'
    Layout = tlCenter
    OnClick = metroLabel1Click
  end
end
