unit tacheformU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, Ttache2013U, zdataset, TmetroSeparatorU,
  TmetrobuttonU, TmetrocheckboxU, TmetroLabelU, TmetroCustomcontrolU, TmetroFormU;

type
  TForm5 = class(TmetroForm)
    Label4: TLabel;
    ComboBox1: TComboBox;
    CheckBox1: TCheckBox;
    DateTimePicker1: TDateTimePicker;
    CheckBox2: TCheckBox;
    DateTimePicker2: TDateTimePicker;
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Memo1: TMemo;
    ScrollBox1: TScrollBox;
    Label3: TLabel;
    Label5: TLabel;
    ComboBox2: TComboBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Label6: TLabel;
    CheckBox3: TCheckBox;
    DateTimePicker3: TDateTimePicker;
    Panel4: TPanel;
    Memo2: TMemo;
    Label10: TLabel;
    ComboBox3: TComboBox;
    Label11: TLabel;
    Label8: TLabel;
    ComboBox4: TComboBox;
    Label9: TLabel;
    metroSeparator1: TmetroSeparator;
    metroSeparator2: TmetroSeparator;
    metroSeparator3: TmetroSeparator;
    metroButton1: TmetroButton;
    metroLabel1: TmetroLabel;
    metroButton3: TmetroButton;
    metroSeparator4: TmetroSeparator;
    metroLabel2: TmetroLabel;
    Label7: TLabel;
    ListBox1: TListBox;
    metroButton2: TmetroButton;
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure metroButton1Click(Sender: TObject);
    procedure metroCheckBox1Click(Sender: TObject);
    procedure metroLabel1Click(Sender: TObject);
    procedure metroButton3Click(Sender: TObject);
    procedure metroLabel2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure metroButton2Click(Sender: TObject);
  private
    { Private declarations }
    titre: string;
    procedure clearCommentaires;
    procedure addFormCommentaire(const q: Tzquery; const tab: integer);
    procedure addCommentaire(const q: Tzquery; const tab: integer);
    procedure listeProjets;
    procedure termine(Sender: TObject);
    procedure listeCategories;
    procedure listePred;
  public
    { Public declarations }
    procedure load(const tache: string);
    procedure listeCommentaires;
  end;

var
  Form5: TForm5;

implementation

uses mainFormU, nouveauCommentaireFormU, commentaireFormU, ajouterPrede;
{$R *.dfm}

procedure TForm5.listeCategories;
begin
  try
    ComboBox4.Items.BeginUpdate;
    ComboBox4.Clear;
    form1.ZQuery1.SQL.Text := 'SELECT titre FROM categories ORDER BY titre';
    form1.ZQuery1.Open;
    form1.ZQuery1.first;
    while not form1.ZQuery1.Eof do
    begin
      ComboBox4.Items.Add(form1.ZQuery1.FieldByName('titre').AsString);
      form1.ZQuery1.Next;
    end;
  finally
    form1.ZQuery1.close;
    ComboBox4.Items.EndUpdate;
  end;
end;

procedure TForm5.CheckBox1Click(Sender: TObject);
begin
  DateTimePicker1.Enabled := CheckBox1.Checked;
end;

procedure TForm5.CheckBox2Click(Sender: TObject);
begin
  DateTimePicker2.Enabled := CheckBox2.Checked;
end;

procedure TForm5.CheckBox3Click(Sender: TObject);
begin
  DateTimePicker3.Enabled := CheckBox3.Checked;
  termine(Sender);
end;

procedure TForm5.termine(Sender: TObject);
begin
  if (TCheckBox(Sender) = CheckBox3) then
  begin
    if TCheckBox(Sender).Checked then
    begin
      ComboBox3.Text := '100';
      DateTimePicker3.DateTime := now;
    end
    else
    begin
      if ComboBox2.Text = '100' then
        ComboBox2.Text := '90';
      ComboBox3.Text := ComboBox2.Text;
    end;
    // Button1Click(Sender);
  end;
  if (TComboBox(Sender) = ComboBox2) then
    ComboBox3.Text := ComboBox2.Text;
  if (TComboBox(Sender) = ComboBox3) then
    ComboBox2.Text := ComboBox3.Text;
  if (ComboBox2.Text = '100') or (ComboBox3.Text = '100') then
  begin
    CheckBox3.Checked := TRUE;
    DateTimePicker3.Enabled := TRUE;
  end
  else
  begin
    CheckBox3.Checked := false;
    DateTimePicker3.Enabled := false;
  end;
  // if (TComboBox(Sender) = ComboBox3) then
  // Button1Click(Sender);
end;

procedure TForm5.load(const tache: string);
var
  cat: string;
begin
  listeProjets;
  form1.ZQuery1.SQL.Text := 'SELECT * FROM taches WHERE titre=:titre';
  form1.ZQuery1.Params.ParamByName('titre').Value := tache;
  try
    form1.ZQuery1.Open;
  except
    on E: Exception do
      showmessage(E.Message);
  end;
  Edit1.Text := form1.ZQuery1.FieldByName('titre').AsString;
  titre := Edit1.Text;
  caption := form1.ZQuery1.FieldByName('projet').AsString + ' - ' + titre;
  Memo1.Text := form1.ZQuery1.FieldByName('description').AsString;
  Memo2.Text := form1.ZQuery1.FieldByName('description').AsString;
  ComboBox1.Text := form1.ZQuery1.FieldByName('projet').AsString;

  DateTimePicker1.DateTime := now();
  DateTimePicker2.DateTime := now();
  DateTimePicker3.DateTime := now();

  if not form1.ZQuery1.FieldByName('datedebut').IsNull then
  begin
    DateTimePicker1.Date := form1.ZQuery1.FieldByName('datedebut').AsDateTime;
  end;
  CheckBox1.Checked := not form1.ZQuery1.FieldByName('datedebut').IsNull;
  DateTimePicker1.Enabled := CheckBox1.Checked;

  if not form1.ZQuery1.FieldByName('datefin').IsNull then
  begin
    DateTimePicker2.Date := form1.ZQuery1.FieldByName('datefin').AsDateTime;
  end;
  CheckBox2.Checked := not form1.ZQuery1.FieldByName('datefin').IsNull;
  DateTimePicker2.Enabled := CheckBox2.Checked;

  if not form1.ZQuery1.FieldByName('datetermine').IsNull then
  begin
    DateTimePicker3.Date := form1.ZQuery1.FieldByName('datetermine').AsDateTime;
  end;
  CheckBox3.Checked := not form1.ZQuery1.FieldByName('datetermine').IsNull;
  DateTimePicker3.Enabled := CheckBox3.Checked;

  ComboBox2.Text := form1.ZQuery1.FieldByName('avancement').AsString;
  ComboBox3.Text := form1.ZQuery1.FieldByName('avancement').AsString;
  termine(nil);
  cat := form1.ZQuery1.FieldByName('categorie').AsString;

  metroLabel1.Visible := form1.ZQuery1.FieldByName('qui').AsString = form1.moi;
  ComboBox3.Enabled := form1.ZQuery1.FieldByName('qui').AsString = form1.moi;

  form1.ZQuery1.close;

  listeCategories;

  ComboBox4.Text := cat;

  listeCommentaires;

  Label9.caption := 'Aujourd''hui le ' + dateToStr(now);

  listePred;

  showModal;
end;

procedure TForm5.Memo1Change(Sender: TObject);
begin
  Memo2.Lines.Assign(Memo1.Lines);
end;

procedure TForm5.metroButton1Click(Sender: TObject);
begin
  form4.new;
  if form4.showModal = 1 then
  begin
    form1.ZQuery1.SQL.Text := 'INSERT INTO commentaires(description,tache,parent,date,qui) VALUES(:desc,:titre,NULL,NOW(),:qui)';
    form1.ZQuery1.Params.ParamByName('desc').Value := form4.Memo1.Text;
    form1.ZQuery1.Params.ParamByName('titre').Value := titre;
    form1.ZQuery1.Params.ParamByName('qui').Value := form1.moi;
    try
      form1.ZQuery1.ExecSQL;
    except
      on E: Exception do
        showmessage(E.Message);
    end;
  end;
  listeCommentaires;
end;

procedure TForm5.metroButton2Click(Sender: TObject);
var
  I: integer;
begin
  form10.preselect(ListBox1.Items);
  if form10.showModal = mrok then
  begin
    ListBox1.Clear;
    for I := 0 to form10.ListBox1.Count - 1 do
      if form10.ListBox1.Selected[I] then
        ListBox1.Items.Add(form10.ListBox1.Items[I]);
  end;
end;

procedure TForm5.listePred;
begin
  ListBox1.Items.BeginUpdate;
  try
    ListBox1.Clear;
    form1.ZQuery1.SQL.Text := 'SELECT * FROM predeceseurs WHERE tache=:tache';
    form1.ZQuery1.ParamByName('tache').Value := titre;
    form1.ZQuery1.Open;
    form1.ZQuery1.first;
    while not form1.ZQuery1.Eof do
    begin
      ListBox1.Items.Add(form1.ZQuery1.FieldByName('predeceseur').AsString);
      form1.ZQuery1.Next;
    end;
  finally
    ListBox1.Items.EndUpdate;
  end;

end;

procedure TForm5.metroButton3Click(Sender: TObject);
var
  I: integer;
begin
  form1.ZQuery1.SQL.Text := 'UPDATE taches SET titre=:titre,description=:description,projet=:projet,avancement=:avancement,dateavancement=:dateavancement,datedebut=:datedebut,datefin=:datefin,datetermine=:datetermine,categorie=:categorie  WHERE titre=:titreoriginal';

  form1.ZQuery1.Params.ParamByName('titre').Value := Edit1.Text;
  form1.ZQuery1.Params.ParamByName('description').Value := Memo1.Text;
  form1.ZQuery1.Params.ParamByName('projet').Value := ComboBox1.Text;
  form1.ZQuery1.Params.ParamByName('avancement').Value := ComboBox2.Text;
  form1.ZQuery1.Params.ParamByName('dateavancement').Value := 'NOW()';
  form1.ZQuery1.Params.ParamByName('categorie').Value := ComboBox4.Text;

  if CheckBox1.Checked then
    form1.ZQuery1.Params.ParamByName('datedebut').Value := dateToStr(DateTimePicker1.DateTime)
  else
    form1.ZQuery1.Params.ParamByName('datedebut').Value := null; // 'NULL';

  if CheckBox2.Checked then
    form1.ZQuery1.Params.ParamByName('datefin').Value := dateToStr(DateTimePicker2.DateTime)
  else
    form1.ZQuery1.Params.ParamByName('datefin').Value := null; // 'NULL';

  if CheckBox3.Checked then
    form1.ZQuery1.Params.ParamByName('datetermine').Value := dateToStr(DateTimePicker3.DateTime)
  else
    form1.ZQuery1.Params.ParamByName('datetermine').Value := null; // 'NULL';

  form1.ZQuery1.Params.ParamByName('titreoriginal').Value := titre;
  try
    form1.ZQuery1.ExecSQL;
  except
    on E: Exception do
      showmessage(E.Message);
  end;
  // supprime tout les pr�d�c�seurs de la t�che
  form1.ZQuery1.SQL.Text := 'DELETE FROM predeceseurs WHERE tache=:tache';
  form1.ZQuery1.ParamByName('tache').Value := Edit1.Text;
  form1.ZQuery1.ExecSQL;
  // ajoute les nouveaux
  for I := 0 to ListBox1.Count - 1 do
  begin
    form1.ZQuery1.SQL.Text := 'INSERT INTO predeceseurs(tache,predeceseur) VALUES(:tache,:pred)';
    form1.ZQuery1.ParamByName('tache').Value := Edit1.Text;
    form1.ZQuery1.ParamByName('pred').Value := ListBox1.Items[I];
    try
      form1.ZQuery1.ExecSQL;
    except
      on E: Exception do
        showmessage(E.Message);
    end;
  end;

  Panel1.Visible := false;

  form1.listeTaches;
  form1.drawProjet;

end;

procedure TForm5.metroCheckBox1Click(Sender: TObject);
begin
  termine(Sender);
end;

procedure TForm5.metroLabel1Click(Sender: TObject);
begin
  Panel1.Visible := TRUE;
end;

procedure TForm5.metroLabel2Click(Sender: TObject);
begin
  Panel1.Visible := false;
end;

procedure TForm5.listeCommentaires;
begin
  clearCommentaires;
  form1.ZQuery1.SQL.Text := 'SELECT * FROM commentaires WHERE tache=:tache AND parent IS NULL ORDER BY id ASC';
  form1.ZQuery1.Params.ParamByName('tache').Value := titre;
  form1.ZQuery1.Open;
  while not form1.ZQuery1.Eof do
  begin
    addCommentaire(form1.ZQuery1, 0);
    form1.ZQuery1.Next;
  end;
  form1.ZQuery1.close;
  Update;
end;

procedure TForm5.addCommentaire(const q: Tzquery; const tab: integer);
var
  zq: Tzquery;
begin
  zq := Tzquery.create(application);
  zq.Connection := form1.ZConnection1;
  zq.SQL.Text := 'SELECT * FROM commentaires WHERE tache=:tache AND parent=:pid ORDER BY id ASC';
  zq.Params.ParamByName('tache').Value := titre;
  zq.Params.ParamByName('pid').Value := q.FieldByName('id').AsInteger;
  zq.Open;
  while not zq.Eof do
  begin
    addCommentaire(zq, tab + 1);
    zq.Next;
  end;
  zq.close;
  zq.free;
  addFormCommentaire(q, tab);
end;

procedure TForm5.addFormCommentaire(const q: Tzquery; const tab: integer);
var
  f: Tform6;
begin
  f := Tform6.create(application);
  f.parent := ScrollBox1;
  f.Align := altop;
  f.AlignWithMargins := TRUE;
  f.Margins.Left := 20 * tab;
  f.Top := 1;
  f.tache := titre;
  f.id := q.FieldByName('id').AsInteger;
  f.Memo2.Text := q.FieldByName('description').AsString;
  f.Label6.caption := humandatetime(q.FieldByName('date').AsDateTime) + ' par ' + q.FieldByName('qui').AsString;
  if not q.FieldByName('datemodif').IsNull then
    f.Label6.caption := f.Label6.caption + ' (derni�re �dition ' + humandatetime(q.FieldByName('datemodif').AsDateTime) + ')';

  f.metroLabel1.Visible := q.FieldByName('qui').AsString = form1.moi;
  f.metroLabel2.Visible := q.FieldByName('qui').AsString = form1.moi;

  f.Visible := TRUE;
end;

procedure TForm5.clearCommentaires;
var
  I: integer;
begin
  for I := ScrollBox1.ControlCount - 1 downto 0 do
  begin
    Tform6(ScrollBox1.Controls[I]).free;
  end;
end;

procedure TForm5.ComboBox2Change(Sender: TObject);
begin
  termine(Sender);
  if ComboBox2.Text = '100' then
  begin
    form1.ZQuery1.SQL.Text := 'UPDATE taches SET avancement=:avancement,datetermine=:datetermine WHERE titre=:titre';
    form1.ZQuery1.Params.ParamByName('titre').Value := Edit1.Text;
    form1.ZQuery1.Params.ParamByName('avancement').Value := ComboBox2.Text;
    form1.ZQuery1.Params.ParamByName('datetermine').Value := dateToStr(DateTimePicker3.DateTime);
  end
  else
  begin
    form1.ZQuery1.SQL.Text := 'UPDATE taches SET avancement=:avancement WHERE titre=:titre';
    form1.ZQuery1.Params.ParamByName('titre').Value := Edit1.Text;
    form1.ZQuery1.Params.ParamByName('avancement').Value := ComboBox2.Text;
  end;
  form1.ZQuery1.ExecSQL;
end;

procedure TForm5.Edit1Change(Sender: TObject);
begin
  caption := ComboBox1.Text + ' - ' + Edit1.Text;
end;

procedure TForm5.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Panel1.Visible := false;
  form1.listeProjets;
  form1.listeTaches;
end;

procedure TForm5.FormCreate(Sender: TObject);
begin
  metroSeparator1.color := $00D4D4D4;
  metroSeparator3.color := $00D4D4D4;
end;

procedure TForm5.listeProjets;
begin
  try
    ComboBox1.Items.BeginUpdate;
    ComboBox1.Clear;
    form1.ZQuery1.SQL.Text := 'SELECT titre FROM projets';
    form1.ZQuery1.Open;
    form1.ZQuery1.first;
    while not form1.ZQuery1.Eof do
    begin
      ComboBox1.Items.Add(form1.ZQuery1.FieldByName('titre').AsString);
      form1.ZQuery1.Next;
    end;
  finally
    form1.ZQuery1.close;
    ComboBox1.Items.EndUpdate;
  end;
end;

end.
