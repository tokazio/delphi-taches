unit mainFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ZAbstractRODataset, ZAbstractDataset, ZDataset,
  ZAbstractConnection, ZConnection, Menus, ActnList,
  PlatformDefaultStyleActnCtrls, ActnMan, ComCtrls, ExtCtrls, StdCtrls, dateutils, ttache2013U,
  ImgList, ToolWin, TmetroFormU, TmetroCustomcontrolU, TmetrobuttonU, TmetroSeparatorU, printers, Math,
  TmetrocheckboxU, OExport, OExport_VclForms, jpeg, TmetroGridU;

type
  TForm1 = class(TmetroForm)
    ZConnection1: TZConnection;
    ZQuery1: TZQuery;
    ActionManager1: TActionManager;
    nouveauprojet: TAction;
    nouvelletache: TAction;
    Splitter1: TSplitter;
    quitter: TAction;
    ListView2: TListView;
    ImageList1: TImageList;
    PopupMenu1: TPopupMenu;
    delprojet: TAction;
    Supprimerleprojet1: TMenuItem;
    PopupMenu2: TPopupMenu;
    deltache: TAction;
    Supprimerlatche1: TMenuItem;
    PaintBox1: TPaintBox;
    Panel1: TPanel;
    Splitter2: TSplitter;
    nouvellecategorie: TAction;
    Panel2: TPanel;
    metroSeparator2: TmetroSeparator;
    metroButton1: TmetroButton;
    metroButton2: TmetroButton;
    metroButton3: TmetroButton;
    imprimer: TAction;
    PrintDialog1: TPrintDialog;
    metroButton4: TmetroButton;
    N1: TMenuItem;
    Editerleprojet1: TMenuItem;
    versexcel: TAction;
    metroButton5: TmetroButton;
    metroSeparator1: TmetroSeparator;
    metroSeparator3: TmetroSeparator;
    Panel4: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Panel5: TPanel;
    Label1: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Panel3: TPanel;
    metroButton6: TmetroButton;
    calendrier: TAction;
    imprimercal: TAction;
    savecal: TAction;
    SaveDialog1: TSaveDialog;
    metroSeparator4: TmetroSeparator;
    TreeView1: TTreeView;
    Splitter3: TSplitter;
    PopupMenu3: TPopupMenu;
    erminer1: TMenuItem;
    Action1: TAction;
    metrogrid1: Tmetrogrid;
    ScrollBox1: TScrollBox;
    procedure FormCreate(Sender: TObject);
    procedure nouveauprojetExecute(Sender: TObject);
    procedure nouvelletacheExecute(Sender: TObject);
    procedure quitterExecute(Sender: TObject);
    procedure ListView2Click(Sender: TObject);
    procedure delprojetExecute(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure deltacheExecute(Sender: TObject);
    procedure PopupMenu2Popup(Sender: TObject);
    procedure PaintBox1Paint(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure nouvellecategorieExecute(Sender: TObject);
    procedure imprimerExecute(Sender: TObject);
    procedure Editerleprojet1Click(Sender: TObject);
    procedure versexcelExecute(Sender: TObject);
    procedure ListView10ColumnClick(Sender: TObject; Column: TListColumn);
    procedure Label1Click(Sender: TObject);
    procedure Splitter2Moved(Sender: TObject);
    procedure calendrierExecute(Sender: TObject);
    procedure savecalExecute(Sender: TObject);
    procedure imprimercalExecute(Sender: TObject);
    procedure TreeView1Deletion(Sender: TObject; Node: TTreeNode);
    procedure TreeView1DblClick(Sender: TObject);
    procedure erminer1Click(Sender: TObject);
    procedure PopupMenu3Popup(Sender: TObject);
    procedure TreeView1Hint(Sender: TObject; const Node: TTreeNode; var Hint: string);
    procedure metrogrid1DblClick(Sender: TObject);
    procedure metrogrid1Deletion(Sender: TObject; Row: Trow);
  private
    { Private declarations }
    procedure readSQL(const q: TZQuery);
    procedure print(const sql: string; const date: TdateTime);
    procedure toExcel(const sql: Tstrings; const date: TdateTime; worksheet: Texportworksheet);
    procedure enteteExcel(d: TdateTime; worksheet: Texportworksheet);
    function delais: Integer;
    procedure statsProjets;
    procedure statsTaches;
    procedure nextTaches;
    procedure addnextTache(const sql: string; p: TTreeNode);
  public
    { Public declarations }
    projetActif: String;
    buffer: Tbitmap;
    moi: string;
    colFilter: string;
    colFilterAsc: boolean;
    affiche: string;
    procedure listeProjets;
    procedure listeTaches;
    procedure drawProjet;
  end;

var
  Form1: TForm1;

const
  INCHPERMM: single = 0.0393701;

implementation

uses nouveauProjetFormU, nouveauCommentaireFormU, nouvelleTacheFormU,
  tacheformU, categorieFormU, choixprintFormU, calFormU;
{$R *.dfm}

function ComputerName: string;
var
  lpBuffer: array [0 .. MAX_COMPUTERNAME_LENGTH] of char;
  nSize: dword;
begin
  nSize := Length(lpBuffer);
  if GetComputerName(lpBuffer, nSize) then
    result := lpBuffer
  else
    result := '';
end;

procedure TForm1.versexcelExecute(Sender: TObject);
var
  xExport: ToExport;
  a: Texportworksheet;
  f: string;
  d: TdateTime;
  s: Tstringlist;
begin
  xExport := ToExport.Create;
  s := Tstringlist.Create;
  try
    // en cours
    a := xExport.AddWorkSheet('En cours');
    enteteExcel(d, a);
    s.Clear;
    s.add('SELECT *,');
    s.add('(SELECT avg(avancement) FROM taches a WHERE a.projet=b.projet GROUP BY projet) as avtot,');
    s.add('(SELECT string_agg(to_char(date,''dd/mm/yyyy'')||'': ''||description,''|'') FROM commentaires WHERE tache=b.titre) as com,');
    s.add('(SELECT string_agg(predeceseur,'','') FROM predeceseurs c WHERE c.tache=b.titre) as predeceseurs');
    s.add(' FROM taches b WHERE avancement<100 ' + f + ' ORDER BY datefin,projet,avancement DESC,titre');
    toExcel(s, d, a);
    // termin�es
    a := xExport.AddWorkSheet('Termin�es');
    enteteExcel(d, a);
    s.Clear;
    s.add('SELECT *,');
    s.add('100 as avtot,');
    s.add('(SELECT string_agg(to_char(date,''dd/mm/yyyy'')||'': ''||description,''|'') FROM commentaires WHERE tache=b.titre) as com,');
    s.add('(SELECT string_agg(predeceseur,'','') FROM predeceseurs c WHERE c.tache=b.titre) as predeceseurs');
    s.add('FROM taches b WHERE avancement=100 ' + f + ' ORDER BY datetermine,projet,avancement,titre,datefin');
    toExcel(s, d, a);
    xExport.SaveToFileWithDialog('T�ches aux ' + formatDateTime('ddmmY', now), '', True);
  finally
    xExport.Free;
  end;
  s.Free;
end;

procedure TForm1.toExcel(const sql: Tstrings; const date: TdateTime; worksheet: Texportworksheet);
var
  I: Integer;
  X, y, w, w2, w3, w4, h: Integer;
  d, projet, p: string;
  c: Tcolor;
begin
  with worksheet do
  begin
    (*
      with AddRow do begin
      AddCellString('string').SetWidth(200);
      AddCellString('my "custom" string').SetWidth(400).SetFontStyle([fsBold]).
      SetAlignment(taCenter).SetBorders(cbAll, ebThick, clGreen).
      SetBorder(cbBottom, ebDouble, clRed);
      AddCellNumber(15, 0);
      AddCellNumber(15.7812, 2);
      AddCellFormulaNumber('B3+100*B4', 2);
      AddCellDate(Now);
      end;
      with AddRow do begin
      AddCellString;
      AddCellString('cell spanned over 3 columns and 2 rows').SetColSpan(3).SetRowSpan(2).
      SetVAlignment(cavCenter).SetAlignment(cahCenter).SetBorders(cbAll, ebMedium, clBlue).
      SetBGColor(clYellow);
      end;

      AddRow.AddCellString('Pie Chart').SetFontSize(20);

      with AddCellString('').AddChart(TExportChartPie, 0, 0, 500, 300) do
      with TExportChartPie(Drawing) do begin
      Title := 'Sales in January in �';
      Fill.Color := clWhite;
      Border.Color := clBlue;
      Shadow.FillStyle := edfColor;
      Form := ecf3D;
      Legend := eclRight;
      XTicsRange.SetRange(0, Rows.Count, 1, 4);
      with AddData(1, Rows.Count, 1, 4) do begin
      Border.FillStyle := edfNone;
      ShowLabels := True;
      end;
      end;
      end;
      *)

    //
    if date <> 0 then
      d := ' depuis le ' + datetostr(date)
    else
      d := '';
    //
    projet := '';
    ZQuery1.sql.Assign(sql);
    ZQuery1.Open;
    ZQuery1.First;
    while not ZQuery1.Eof do
    begin
      // En-t�te projet
      if projet <> ZQuery1.FieldByName('projet').AsString then
      begin
        addRow;
        with addRow do
        begin
          addCellString(ZQuery1.FieldByName('datefin').AsString).SetBGColor($00783C).setFontColor(clWhite);
          addCellString('').SetBGColor($00783C).setFontColor(clWhite);
          addCellString(ZQuery1.FieldByName('projet').AsString).SetBGColor($00783C).setFontColor(clWhite);
          addCellString('').SetBGColor($00783C).setFontColor(clWhite);
          addCellString('').SetBGColor($00783C).setFontColor(clWhite);
          addCellString('').SetBGColor($00783C).setFontColor(clWhite);
          addCellPercent(ZQuery1.FieldByName('avtot').AsInteger / 100).SetBGColor($00783C).setFontColor(clWhite);
          addCellString('').SetBGColor($00783C).setFontColor(clWhite);
          projet := ZQuery1.FieldByName('projet').AsString;
          addCellString('').SetBGColor($00783C).setFontColor(clWhite);
          addCellString('').SetBGColor($00783C).setFontColor(clWhite);
          addCellString('').SetBGColor($00783C).setFontColor(clWhite);
          addCellString('').SetBGColor($00783C).setFontColor(clWhite);
        end;
      end;

      // if ZQuery1.FieldByName('avancement').AsInteger = 100 then color := clgreen;

      // T�che
      with addRow do
      begin
        c := clblack;
        if delais < 0 then
          if ZQuery1.FieldByName('predeceseurs').AsString = '' then
            c := clred;

        addCellString(ZQuery1.FieldByName('datefin').AsString).setFontColor(c);
        addCellString('');
        addCellString('');
        addCellString(ZQuery1.FieldByName('titre').AsString).setFontColor(c);
        addCellString('');
        addCellString('');
        addCellPercent(ZQuery1.FieldByName('avancement').AsInteger / 100).setFontColor(c);
        addCellString(ZQuery1.FieldByName('datetermine').AsString).setFontColor(c);
        addCellString('');
        if delais < 0 then
        begin
          if ZQuery1.FieldByName('predeceseurs').AsString = '' then
            addCellString('en retard').setFontColor(c)
          else
            addCellString('Apr�s: ' + ZQuery1.FieldByName('predeceseurs').AsString).setFontColor(c).SetWrapText(True);
        end
        else if delais > 0 then
          addCellString('en avance').setFontColor(c)
        else
          addCellString('');
        addCellString('');
        addCellString(stringReplace(ZQuery1.FieldByName('com').AsString, '|', #13, [rfReplaceAll, rfIgnoreCase])).SetWrapText(True).SetCalculateRowHeight(erhMultiLine);
      end;

      if ZQuery1.FieldByName('description').AsString <> '' then
        with addRow do
        begin
          addCellString('');
          addCellString('');
          addCellString('');
          addCellString('');
          addCellString(ZQuery1.FieldByName('description').AsString).setFontColor(clGray).SetWrapText(True).SetCalculateRowHeight(erhMultiLine);
        end;

      ZQuery1.Next;
    end;
    ZQuery1.Close;
  end;
end;

procedure TForm1.TreeView1DblClick(Sender: TObject);
begin
  if TreeView1.Selected = nil then
    exit;
  if TreeView1.Selected.Data = nil then
    exit;
  form5.load(Tligne(TreeView1.Selected.Data)['titre']);
end;

procedure TForm1.TreeView1Deletion(Sender: TObject; Node: TTreeNode);
begin
  if Node.Data <> nil then
  begin
    Tligne(Node.Data).Free;
    Node.Data := nil;
  end;
end;

procedure TForm1.TreeView1Hint(Sender: TObject; const Node: TTreeNode; var Hint: string);
begin
  if Node.Data <> nil then
    Hint := 'Projet: ' + Tligne(Node.Data)['projet']
end;

procedure TForm1.calendrierExecute(Sender: TObject);
begin
  form9.show;
end;

function TForm1.delais: Integer;
begin
  result := 0;
  // termin�
  if (not ZQuery1.FieldByName('datefin').IsNull) and not ZQuery1.FieldByName('datetermine').IsNull then
  begin
    if ZQuery1.FieldByName('datetermine').AsDateTime > ZQuery1.FieldByName('datefin').AsDateTime then
      result := -1;
    if ZQuery1.FieldByName('datetermine').AsDateTime < ZQuery1.FieldByName('datefin').AsDateTime then
      result := 1;
  end
  // en cours
  else if (not ZQuery1.FieldByName('datefin').IsNull) then
  begin
    if ZQuery1.FieldByName('datefin').AsDateTime < now then
      result := -1;
  end;
end;

procedure TForm1.enteteExcel(d: TdateTime; worksheet: Texportworksheet);
begin
  with worksheet do
  begin
    // TITRE de la page
    with addRow do
    begin
      addCellString('CR des T�ches. Le ' + datetostr(now) + ' (T�ches2013 version ' + applicationVersion + ')').SetFontStyle([fsbold]).SetFontSize(22);
    end;
    addRow;
    // En-t�tes des colones
    with addRow do
    begin
      addCellString('Pour le').SetFontStyle([fsbold]).SetWidth(80);
      addCellString('').SetFontStyle([fsbold]).SetWidth(20);
      addCellString('Projet/T�che/Description').SetFontStyle([fsbold]).SetWidth(20);
      addCellString('').SetFontStyle([fsbold]).SetWidth(20);
      addCellString('').SetFontStyle([fsbold]).SetWidth(400);
      addCellString('').SetFontStyle([fsbold]).SetWidth(20);
      addCellString('Avancement').SetFontStyle([fsbold]).SetWidth(100);
      addCellString('Termin� le').SetFontStyle([fsbold]).SetWidth(80);
      addCellString('').SetFontStyle([fsbold]).SetWidth(20);
      addCellString('Etat').SetFontStyle([fsbold]).SetWidth(100);
      addCellString('').SetFontStyle([fsbold]).SetWidth(20);
      addCellString('Commentaires').SetFontStyle([fsbold]).SetWidth(400);
    end;
    addRow;
  end;
end;

procedure TForm1.erminer1Click(Sender: TObject);
begin
  if TreeView1.Selected = nil then
    exit;
  if TreeView1.Selected.Data = nil then
    exit;
  ZQuery1.sql.Text := 'UPDATE taches SET avancement=100,datetermine=NOW() WHERE titre=:titre';
  ZQuery1.ParamByName('titre').Value := Tligne(TreeView1.Selected.Data)['titre'];
  ZQuery1.ExecSQL;
  listeProjets;
  listeTaches;
end;

procedure TForm1.delprojetExecute(Sender: TObject);
var
  buttonSelected: Integer;
begin
  if ListView2.Selected = nil then
    exit;
  buttonSelected := MessageDlg('Etes vous s�r de vouloir supprimer le projet ' + ListView2.Selected.Caption + ' et toutes ses t�ches?', mtCustom, [mbYes, mbNo], 0);
  if buttonSelected = mrYes then
  begin
    ZQuery1.sql.Text := 'DELETE FROM projets WHERE titre=:titre AND qui=:qui';
    ZQuery1.Params.ParamByName('titre').Value := ListView2.Selected.Caption;
    ZQuery1.Params.ParamByName('qui').Value := moi;
    try
      ZQuery1.ExecSQL;
    except
      on E: Exception do
        showmessage(E.Message);
    end;
    if ListView2.Selected.Caption = projetActif then
    begin
      projetActif := '';
      listeTaches;
    end;
    listeProjets;
  end;
end;

procedure TForm1.deltacheExecute(Sender: TObject);
var
  buttonSelected: Integer;
begin
  (*
    if ListView1.Selected = nil then
    exit;
    buttonSelected := MessageDlg('Etes vous s�r de vouloir supprimer la t�che ' + ListView1.Selected.Caption + ' et tout ses commentaires?', mtCustom, [mbYes, mbNo], 0);
    if buttonSelected = mrYes then
    begin
    ZQuery1.sql.Text := 'DELETE FROM taches WHERE titre=:titre AND qui=:qui';
    ZQuery1.Params.ParamByName('titre').Value := ListView1.Selected.Caption;
    ZQuery1.Params.ParamByName('qui').Value := moi;
    try
    ZQuery1.ExecSQL;
    except
    on E: Exception do
    showmessage(E.Message);
    end;
    listeTaches;
    end;
    *)
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  c: Tstringlist;
  f: string;
begin
  metroSeparator1.color := $00D4D4D4;
  metroSeparator3.color := $00D4D4D4;
  metroSeparator4.color := $00D4D4D4;
  PaintBox1.ControlStyle := PaintBox1.ControlStyle + [csOpaque];
  moi := ComputerName;
  Caption := 'T�ches - ' + moi;
  buffer := Tbitmap.Create;
  c := Tstringlist.Create;
  f := extractfilepath(application.ExeName) + 'config.txt';
  if fileexists(f) then
  begin
    c.loadFromfile(f);
    ZConnection1.HostName := c.Values['host'];
    ZConnection1.Port := strtoint(c.Values['port']);
    ZConnection1.database := c.Values['database'];
    ZConnection1.User := c.Values['user'];
    ZConnection1.Password := c.Values['password'];
  end;
  ZConnection1.Connect;
  c.Free;
  Panel1.Align := alClient;
  Label1Click(Label7);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  buffer.Free;
end;

function br(const y, h: Integer): Integer;
begin
  result := y + h;
  if result > printer.Canvas.ClipRect.Bottom - 200 then
  begin
    printer.NewPage;
    result := 100;
  end;

end;

procedure setPrinterA4;
var
  Device, Driver, Port: array [0 .. 80] of char;
  DevMode: THandle;
  pDevmode: PDeviceMode;
begin
  { Get printer device name etc. }
  printer.GetPrinter(Device, Driver, Port, DevMode);
  { force reload of DEVMODE }
  printer.SetPrinter(Device, Driver, Port, 0);
  { get DEVMODE handle }
  printer.GetPrinter(Device, Driver, Port, DevMode);
  if DevMode <> 0 then
  begin
    { lock it to get pointer to DEVMODE record }
    pDevmode := GlobalLock(DevMode);
    if pDevmode <> nil then
      try
        with pDevmode^ do
        begin
          { modify paper size }
          dmPapersize := DMPAPER_A4;
          { tell printer driver that dmPapersize field contains data it needs to inspect }
          dmFields := dmFields or DM_PAPERSIZE;
        end;
      finally
        { unlock DEVMODE handle }
        GlobalUnlock(DevMode);
      end;
  end;
end;

procedure TForm1.imprimercalExecute(Sender: TObject);
begin
  setPrinterA4;
  printer.Orientation := poLandscape;
  if PrintDialog1.Execute then
  begin
    with printer do
    begin
      beginDoc;
      form9.draw(printer.Canvas, True);
      endDoc;
    end;
    form9.PaintBox1.Repaint;
  end;
end;

procedure TForm1.imprimerExecute(Sender: TObject);
var
  f: string;
  d: TdateTime;
begin
  if form8.showmodal = 1 then
  begin
    f := '';
    if form8.metroCheckBox3.checked then
    begin
      f := 'and datedebut>''' + datetostr(form8.DateTimePicker1.DateTime) + ''' ';
      d := form8.DateTimePicker1.DateTime;
    end
    else
      d := 0;
    if form8.metroCheckBox1.checked then
      if form8.metroCheckBox2.checked then
        // en cours et termin�es
        print('SELECT * FROM taches WHERE 1=1 ' + f + ' ORDER BY projet,avancement,titre', d)
        // en cours
      else
        print('SELECT * FROM taches WHERE avancement<100 ' + f + ' ORDER BY projet,avancement,titre', d)
        // termin�es
      else if form8.metroCheckBox2.checked then
        print('SELECT * FROM taches WHERE avancement=100 ' + f + ' ORDER BY projet,avancement,titre', d)
  end;
end;

procedure TForm1.Label1Click(Sender: TObject);
begin
  if Sender = Label1 then
  begin
    affiche := 'encours';
    TLabel(Sender).Font.color := $0000783C;
    Label5.Font.color := $00444444;
    Label6.Font.color := $00444444;
    Label7.Font.color := $00444444;
  end;

  if Sender = Label5 then
  begin
    affiche := 'enretard';
    TLabel(Sender).Font.color := $0000783C;
    Label1.Font.color := $00444444;
    Label6.Font.color := $00444444;
    Label7.Font.color := $00444444;
  end;

  if Sender = Label6 then
  begin
    affiche := 'terminees';
    TLabel(Sender).Font.color := $0000783C;
    Label1.Font.color := $00444444;
    Label5.Font.color := $00444444;
    Label7.Font.color := $00444444;
  end;

  if Sender = Label7 then
  begin
    affiche := 'encours';
    colFilter := 'datefin';
    colFilterAsc := True;
    TLabel(Sender).Font.color := $0000783C;
    Label1.Font.color := $00444444;
    Label5.Font.color := $00444444;
    Label6.Font.color := $00444444;
  end;

  listeProjets;
  listeTaches;
end;

procedure TForm1.print(const sql: string; const date: TdateTime);
var
  I: Integer;
  X, y, w, w2, w3, w4, h: Integer;
  d, projet, p: string;
begin
  if PrintDialog1.Execute then
    With printer do
    begin
      beginDoc;
      y := 100;
      X := 100;
      Canvas.Font.Name := 'Segoe UI Light';
      Canvas.Font.Size := 12;
      Canvas.brush.style := bsclear;
      Canvas.pen.color := clblack;
      h := ceil(Canvas.TextHeight('"q') * 1.1);
      //
      if date <> 0 then
        d := ' depuis le ' + datetostr(date)
      else
        d := '';

      Canvas.TextOut(X, y, 'CR des T�ches ' + d + ' . Le ' + datetostr(now) + ' (T�ches2013 version ' + applicationVersion + ')');
      y := br(y, h);
      //
      projet := '';
      ZQuery1.sql.Text := sql;
      ZQuery1.Open;
      ZQuery1.First;
      while not ZQuery1.Eof do
      begin

        if projet <> ZQuery1.FieldByName('projet').AsString then
        begin
          X := 100;
          Canvas.Font.style := [fsbold];
          Canvas.Font.color := clblack;
          y := br(y, h);
          Canvas.TextOut(X, y, ZQuery1.FieldByName('projet').AsString);
          Canvas.Font.style := [];
          y := br(y, h);
          Canvas.pen.color := clblack;
          Canvas.MoveTo(X, y);
          Canvas.LineTo(Canvas.ClipRect.Right - X, y);
          projet := ZQuery1.FieldByName('projet').AsString;
        end;

        X := 200;
        if ZQuery1.FieldByName('avancement').AsInteger = 100 then
          Canvas.Font.color := clgreen;

        p := ZQuery1.FieldByName('titre').AsString;
        if ZQuery1.FieldByName('avancement').AsInteger < 100 then
          p := p + ' (' + ZQuery1.FieldByName('avancement').AsString + '%)';
        if ZQuery1.FieldByName('datedebut').AsString <> '' then
        begin
          p := p + ' du ' + ZQuery1.FieldByName('datedebut').AsString;
          if ZQuery1.FieldByName('datefin').AsString <> '' then
            p := p + ' au ' + ZQuery1.FieldByName('datefin').AsString;
        end
        else
        begin
          if ZQuery1.FieldByName('datefin').AsString <> '' then
            p := p + ' pour le ' + ZQuery1.FieldByName('datefin').AsString;
        end;
        if ZQuery1.FieldByName('datetermine').AsString <> '' then
          p := p + ' termin� le ' + ZQuery1.FieldByName('datetermine').AsString;

        if (not ZQuery1.FieldByName('datefin').IsNull) and not ZQuery1.FieldByName('datetermine').IsNull then
        begin
          if ZQuery1.FieldByName('datetermine').AsDateTime > ZQuery1.FieldByName('datefin').AsDateTime then
            p := p + ' (en retard)';
          if ZQuery1.FieldByName('datetermine').AsDateTime < ZQuery1.FieldByName('datefin').AsDateTime then
            p := p + ' (en avance)';
        end;

        Canvas.pen.color := clGray;

        if ZQuery1.FieldByName('avancement').AsInteger = 100 then
        begin
          Canvas.pen.color := clgreen;
          Canvas.MoveTo(X - 65, y + round(h / 3) + 30);
          Canvas.LineTo(X - 50, y + round(h / 3) + 45);
          Canvas.LineTo(X - 37, y + round(h / 3) + 12);
        end;

        Canvas.Rectangle(X - 80, y + round(h / 3), X - 20, y + round(h / 3) + 60);

        Canvas.TextOut(X, y, p);

        y := br(y, h);
        if ZQuery1.FieldByName('description').AsString <> '' then
        begin
          Canvas.Font.Size := 10;
          Canvas.Font.style := [fsitalic];
          X := 300;
          Canvas.TextOut(X, y, ZQuery1.FieldByName('description').AsString);
          Canvas.Font.Size := 12;
          Canvas.Font.style := [];
          y := br(y, h);
        end;
        ZQuery1.Next;
      end;
      ZQuery1.Close;
      endDoc;
    end;
end;

procedure TForm1.listeProjets;
var
  it, ittot: TListItem;
  w: String;
begin
  try
    ListView2.Items.BeginUpdate;
    ListView2.Items.Clear;
    ittot := ListView2.Items.add;
    ittot.Caption := 'Tous';
    ittot.ImageIndex := 223;

    if affiche <> 'terminees' then
      w := 'AND ((SELECT ROUND(AVG(avancement)) FROM taches WHERE taches.projet=projets.titre)<100)';

    ZQuery1.sql.Text := 'SELECT titre,(SELECT ROUND(AVG(avancement)) FROM taches WHERE taches.projet=projets.titre) AS "avancement" FROM projets WHERE 1=1 ' + w + ' ORDER BY avancement';
    try
      ZQuery1.Open;
    except
      on E: Exception do
        showmessage(E.Message);
    end;

    ZQuery1.First;
    while not ZQuery1.Eof do
    begin
      it := ListView2.Items.add;
      it.Caption := ZQuery1.FieldByName('titre').AsString;
      it.ImageIndex := 223;
      it.SubItems.add(ZQuery1.FieldByName('avancement').AsString + '%');
      ZQuery1.Next;
    end;
    ZQuery1.Close;
    // % total
    ZQuery1.sql.Text := 'SELECT ROUND(AVG(avancement)) AS total FROM taches';
    try
      ZQuery1.Open;
    except
      on E: Exception do
        showmessage(E.Message);
    end;
    ittot.SubItems.add(ZQuery1.FieldByName('total').AsString + '%');
    ZQuery1.Close;
  finally
    ListView2.Items.EndUpdate;
  end;
  statsProjets;
end;

procedure TForm1.statsProjets;
begin
  ZQuery1.sql.Text := 'SELECT (SELECT COUNT(*) FROM (SELECT (SELECT ROUND(AVG(avancement)) FROM taches WHERE taches.projet=projets.titre) AS "avancement" FROM projets) as a WHERE avancement<100) as tot';
  ZQuery1.Open;
  ZQuery1.First;
  Label2.Caption := 'Projets en cours: ' + ZQuery1.FieldByName('tot').AsString;
  ZQuery1.Close;
end;

procedure TForm1.listeTaches;
var
  f, o, s: string;
begin
  f := '';
  if affiche = 'encours' then
  begin
    f := ' and avancement<100';
  end
  else if affiche = 'terminees' then
  begin
    f := ' and avancement=100';
  end;
  if affiche = 'enretard' then
  begin
    f := f + ' and datefin is not null and datefin<now() and avancement<100';
  end;

  if colFilterAsc then
    s := 'ASC'
  else
    s := 'DESC';

  if colFilter = 'datefin' then
    o := 'datefin ' + s + ',avancement,titre'
  else if colFilter = 'avancement' then
    o := 'avancement ' + s + ',datefin,titre'
  else if colFilter = 'titre' then
    o := 'titre ' + s + ',avancement,datefin'
  else if colFilter = 'projet' then
    o := 'projet ' + s + ',avancement,titre,datefin'
  else
    o := 'avancement,titre,datefin';

  if projetActif <> '' then
  begin
    ZQuery1.sql.Text := 'SELECT titre,avancement,datedebut,datetermine,datefin,categorie,(SELECT string_agg(predeceseur,'','') FROM predeceseurs b WHERE b.tache=a.titre) as predeceseurs';
    ZQuery1.sql.add('FROM taches a WHERE projet=:projet ' + f + ' ORDER BY ' + o);
    ZQuery1.Params.ParamByName('projet').Value := projetActif;
  end
  else
    ZQuery1.sql.Text := 'SELECT titre,avancement,datedebut,datetermine,datefin,projet,categorie,(SELECT string_agg(predeceseur,'','') FROM predeceseurs b WHERE b.tache=a.titre) as predeceseurs FROM taches a WHERE 1=1 ' + f + ' ORDER BY ' + o;
  try
    ZQuery1.Open;
  except
    on E: Exception do
      showmessage(E.Message);
  end;
  readSQL(ZQuery1);
  ZQuery1.Close;
  statsTaches;
  nextTaches;
end;

procedure TForm1.nextTaches;
var
  n: TTreeNode;
begin
  TreeView1.Items.BeginUpdate;
  TreeView1.Items.Clear;

  n := TreeView1.Items.AddChild(nil, 'Avant');
  addnextTache('SELECT titre,projet FROM taches WHERE datefin<current_date-interval ''1 day'' AND avancement<100 ORDER BY datefin,titre', n);

  n := TreeView1.Items.AddChild(nil, 'Hier');
  addnextTache('SELECT titre,projet FROM taches WHERE datefin=current_date-interval ''1 day'' AND avancement<100 ORDER BY datefin,titre', n);

  n := TreeView1.Items.AddChild(nil, 'Aujourd''hui');
  addnextTache('SELECT titre,projet FROM taches WHERE datefin=current_date AND avancement<100 ORDER BY datefin,titre', n);

  n := TreeView1.Items.AddChild(nil, 'Demain');
  addnextTache('SELECT titre,projet FROM taches WHERE datefin=current_date+interval ''1 day'' AND avancement<100 ORDER BY datefin,titre', n);

  n := TreeView1.Items.AddChild(nil, 'Plus tard');
  addnextTache('SELECT titre,projet FROM taches WHERE datefin>current_date+interval ''1 day'' AND avancement<100 ORDER BY datefin,titre', n);

  TreeView1.Items.EndUpdate;
  TreeView1.FullExpand;
end;

procedure TForm1.addnextTache(const sql: string; p: TTreeNode);
var
  n: TTreeNode;
begin
  ZQuery1.sql.Text := sql;
  ZQuery1.Open;
  ZQuery1.First;
  while not ZQuery1.Eof do
  begin
    n := TreeView1.Items.AddChildObject(p, ZQuery1.FieldByName('titre').AsString, Tligne.Create(ZQuery1.Fields, ZQuery1.FieldDefs));
    ZQuery1.Next;
  end;
  ZQuery1.Close;
end;

procedure TForm1.statsTaches;
begin
  if projetActif <> '' then
  begin
    ZQuery1.sql.Text := 'SELECT (SELECT COUNT(*) FROM taches WHERE avancement<100) as tottache,(SELECT COUNT(*) FROM taches WHERE avancement<100 AND projet=:projet) as nbtache';
    ZQuery1.Params.ParamByName('projet').Value := projetActif;
  end
  else
    ZQuery1.sql.Text := 'SELECT (SELECT COUNT(*) FROM taches WHERE avancement<100) as tottache,(SELECT COUNT(*) FROM taches WHERE avancement<100) as nbtache';
  ZQuery1.Open;
  ZQuery1.First;
  Label3.Caption := 'T�ches en cours: ' + ZQuery1.FieldByName('nbtache').AsString;
  Label4.Caption := 'T�ches en cours au total: ' + ZQuery1.FieldByName('tottache').AsString;
  ZQuery1.Close;
end;

procedure TForm1.drawProjet;
var
  X, maxwt, y, h, s, w, wd, wa, wm, hm: Integer;
  I: Integer;
begin
  if projetActif = '' then
    exit;

  ZQuery1.sql.Text := 'SELECT titre,avancement,datedebut,datetermine,datefin,(datetermine-datedebut)+1 AS duree,(datefin-datedebut)+1 AS delais,';
  ZQuery1.sql.add('datedebut-(SELECT MIN(datedebut) FROM taches WHERE projet=:projet GROUP BY projet) AS start ');
  ZQuery1.sql.add('FROM taches WHERE projet=:projet ORDER BY datedebut');
  ZQuery1.Params.ParamByName('projet').Value := projetActif;

  try
    ZQuery1.Open;
  except
    on E: Exception do
      showmessage(E.Message);
  end;

  // readSQL(ZQuery1);

  // calcul largeur max et hauteur max
  ZQuery1.First;
  with buffer.Canvas do
  begin
    Font.Assign(PaintBox1.Font);
    h := TextHeight('"q') + 2;
    hm := round((h * (ZQuery1.RecordCount + 1)) + h + h + 40);
    wm := round((now - ZQuery1.FieldByName('datedebut').AsDateTime) * 20);
  end;
  PaintBox1.Width := wm;
  PaintBox1.Height := hm;
  buffer.SetSize(wm, hm);
  with buffer.Canvas do
  begin
    // efface
    brush.color := clWhite;
    pen.color := clWhite;
    Rectangle(ClipRect);

    y := h + 20;
    X := 10;
    maxwt := 0;
    // nom des t�ches
    brush.color := $E9E9E9;
    pen.color := brush.color;
    brush.style := bsclear;
    ZQuery1.First;
    while not ZQuery1.Eof do
    begin
      TextOut(X, y, ZQuery1.FieldByName('titre').AsString);
      if textWidth(ZQuery1.FieldByName('titre').AsString) > maxwt then
        maxwt := textWidth(ZQuery1.FieldByName('titre').AsString);
      MoveTo(0, y + h + 1);
      LineTo(ClipRect.Right, y + h + 1);
      y := y + h + 3;
      ZQuery1.Next;
    end;
    ZQuery1.First;
    // cadrillage vertical
    I := 0;
    pen.style := pssolid;
    while maxwt + 20 + I * 10 < buffer.Width do
    begin
      brush.color := $E9E9E9;
      pen.color := brush.color;
      if I = round((now - ZQuery1.FieldByName('datedebut').AsDateTime)) then
      begin
        Font.color := clred;
        brush.style := bsclear;
        TextOut(maxwt + 20 + I * 10, 10, 'Aujourd''hui');
        Font.color := PaintBox1.Font.color;
        brush.style := bssolid;
        brush.color := clred;
      end;
      pen.color := brush.color;
      MoveTo(maxwt + 20 + I * 10, h + 20);
      LineTo(maxwt + 20 + I * 10, buffer.Height);
      I := I + 1;
    end;

    // bandes
    y := h + 20;
    X := maxwt + 20;
    brush.style := bsclear;
    // d�but
    TextOut(X - textWidth('D�but ' + ZQuery1.FieldByName('datedebut').AsString), 10, 'D�but ' + ZQuery1.FieldByName('datedebut').AsString);
    brush.style := bssolid;

    while not ZQuery1.Eof do
    begin
      if not ZQuery1.FieldByName('datedebut').IsNull then
      begin
        s := ZQuery1.FieldByName('start').AsInteger * 10;
        w := ZQuery1.FieldByName('duree').AsInteger * 10;
        wd := ZQuery1.FieldByName('delais').AsInteger * 10;
        wa := round((now - ZQuery1.FieldByName('datedebut').AsDateTime)) * 10;
        // cadre pour le delais
        if not ZQuery1.FieldByName('delais').IsNull then
        begin
          // cadre gris fond blanc
          pen.color := $D9D9D9;
          brush.color := clWhite;
          //
          roundRect(X + s, y, X + s + wd, y + h, 5, 5);
        end;

        // bande si pas termin�
        if (ZQuery1.FieldByName('datetermine').IsNull) then
        begin
          // cadre gris fond en bleu du d�but � aujourd'hui
          brush.color := $FFC176;
          pen.color := $D9D9D9;
          //
          roundRect(X + s, y, X + s + wa, y + h, 5, 5);
          // si en retard: rouge sinon vert
          if (not ZQuery1.FieldByName('datefin').IsNull) and (ZQuery1.FieldByName('datefin').AsDateTime < now) then
            brush.color := $769EFF
          else
            brush.color := $76FFC1;
          // bande de l'avancement en % par dessus la bande bleu
          if ZQuery1.FieldByName('avancement').AsInteger > 0 then
          begin
            pen.style := psclear;
            roundRect(X + s, y + h - 6, X + s + round(wa * (ZQuery1.FieldByName('avancement').AsInteger / 100)), y + h - 1, 5, 5);
            Font.Size := 6;
            brush.style := bsclear;
            TextOut(X + s + round(wa * (ZQuery1.FieldByName('avancement').AsInteger / 100)), y + h - 10, ZQuery1.FieldByName('avancement').AsString + '%');
            Font.Assign(PaintBox1.Font);
          end;
        end
        else
        // bande si termin�
        begin
          // si termin� en retard: texte rouge fond vert (sinon texte blanc fond vert)
          if (not ZQuery1.FieldByName('datefin').IsNull) and (ZQuery1.FieldByName('datefin').AsDateTime < ZQuery1.FieldByName('datetermine').AsDateTime) then
          begin
            brush.color := $769EFF;
            Font.color := clred;
          end
          else
          begin
            brush.color := $76FFC1;
            Font.color := clblack;
          end;
          // cadre gris
          pen.color := $D9D9D9;
          //
          roundRect(X + s, y, X + s + w, y + h, 5, 5);
        end;
        // ecrit la dur�e
        brush.style := bsclear;
        if ZQuery1.FieldByName('duree').IsNull then
          TextOut(X + s + 5, y, inttostr(round(now - ZQuery1.FieldByName('datedebut').AsDateTime)) + 'j')
        else
          TextOut(X + s + 5, y, ZQuery1.FieldByName('duree').AsString + 'j');
        // couleur de texte par d�faut
        Font.color := PaintBox1.Font.color;
        brush.style := bssolid;
        // date de fin (si il y a une datefin et que c'est pas termin� et que je suis en retard)
        if (not ZQuery1.FieldByName('datefin').IsNull) and (ZQuery1.FieldByName('datetermine').IsNull) and (ZQuery1.FieldByName('datefin').AsDateTime < now) then
        begin
          pen.style := pssolid;
          pen.color := clred;
          MoveTo(X + s + wd, y - 2);
          LineTo(X + s + wd, y + h + 2);
        end;
      end;
      y := y + h + 3;
      ZQuery1.Next;
    end;
  end;
  ZQuery1.Close;
  PaintBox1.Repaint;
end;

procedure TForm1.Editerleprojet1Click(Sender: TObject);
begin
  if ListView2.Selected = nil then
    exit;
  form3.Clear;
  ZQuery1.sql.Text := 'SELECT * FROM projets WHERE titre=''' + ListView2.Selected.Caption + '''';
  try
    ZQuery1.Open;
  except
    on E: Exception do
    begin
      showmessage(E.Message);
      exit;
    end;
  end;
  if ZQuery1.RecordCount = 0 then
  begin
    showmessage('Impossible de trouver le projet' + #13 + ZQuery1.sql.Text + #13 + 'Ne retourne aucun r�sultat');
    exit;
  end;
  ZQuery1.First;
  form3.ancienTitre := ListView2.Selected.Caption;
  form3.Edit1.Text := ZQuery1.FieldByName('titre').AsString;
  form3.memo1.Text := ZQuery1.FieldByName('description').AsString;
  form3.combobox1.Text := ZQuery1.FieldByName('categorie').AsString;
  if not ZQuery1.FieldByName('datedebut').IsNull then
  begin
    form3.CheckBox1.checked := True;
    form3.DateTimePicker1.DateTime := ZQuery1.FieldByName('datedebut').AsDateTime;
  end;
  if not ZQuery1.FieldByName('datefin').IsNull then
  begin
    form3.CheckBox2.checked := True;
    form3.DateTimePicker2.DateTime := ZQuery1.FieldByName('datefin').AsDateTime;
  end;
  ZQuery1.Close;
  if form3.showmodal = mrOk then
  begin
    ZQuery1.sql.Text := 'UPDATE projets SET titre=:titre,description=:description,datedebut=:datedebut,datefin=:datefin,categorie=:categorie,qui=:qui WHERE titre=:ancientitre';
    ZQuery1.Params.ParamByName('ancientitre').Value := form3.ancienTitre;
    ZQuery1.Params.ParamByName('titre').Value := form3.Edit1.Text;
    ZQuery1.Params.ParamByName('description').Value := form3.memo1.Text;
    ZQuery1.Params.ParamByName('categorie').Value := form3.combobox1.Text;
    ZQuery1.Params.ParamByName('qui').Value := moi;
    if form3.CheckBox1.checked then
      ZQuery1.Params.ParamByName('datedebut').Value := datetostr(form3.DateTimePicker1.DateTime)
    else
      ZQuery1.Params.ParamByName('datedebut').Value := null;

    if form3.CheckBox2.checked then
      ZQuery1.Params.ParamByName('datefin').Value := datetostr(form3.DateTimePicker2.DateTime)
    else
      ZQuery1.Params.ParamByName('datefin').Value := null;
    try
      ZQuery1.ExecSQL;
    except
      on E: Exception do
        showmessage(E.Message);
    end;
    listeProjets;
  end;
end;

procedure TForm1.ListView10ColumnClick(Sender: TObject; Column: TListColumn);
begin
  if colFilter = Column.Caption then
    colFilterAsc := not colFilterAsc
  else
  begin
    colFilter := Column.Caption;
    colFilterAsc := True;
  end;
  listeTaches;
end;

procedure TForm1.ListView2Click(Sender: TObject);
begin
  if ListView2.Selected = nil then
    projetActif := ''
  else if ListView2.Selected.Caption = 'Tous' then
    projetActif := ''
  else
    projetActif := ListView2.Selected.Caption;
  listeTaches;
  drawProjet;
end;

procedure TForm1.metrogrid1DblClick(Sender: TObject);
begin
  if metrogrid1.Selected = nil then
    exit;
  if metrogrid1.Selected.Data = nil then
    exit;
  form5.load(Tligne(metrogrid1.Selected.Data)['titre']);
end;

procedure TForm1.metrogrid1Deletion(Sender: TObject; Row: Trow);
begin
  if Row.Data <> nil then
  begin
    Tligne(Row.Data).Free;
    Row.Data := nil;
  end;
end;

procedure TForm1.nouveauprojetExecute(Sender: TObject);
begin
  if form3.showmodal = mrOk then
  begin
    ZQuery1.sql.Text := 'INSERT INTO projets(titre,description,datedebut,datefin,categorie,qui) VALUES(:titre,:description,:datedebut,:datefin,:categorie,:qui)';
    ZQuery1.Params.ParamByName('titre').Value := form3.Edit1.Text;
    ZQuery1.Params.ParamByName('description').Value := form3.memo1.Text;
    ZQuery1.Params.ParamByName('categorie').Value := form3.combobox1.Text;
    ZQuery1.Params.ParamByName('qui').Value := moi;
    if form3.CheckBox1.checked then
      ZQuery1.Params.ParamByName('datedebut').Value := datetostr(form2.DateTimePicker1.DateTime)
    else
      ZQuery1.Params.ParamByName('datedebut').Value := null;

    if form3.CheckBox2.checked then
      ZQuery1.Params.ParamByName('datefin').Value := datetostr(form2.DateTimePicker2.DateTime)
    else
      ZQuery1.Params.ParamByName('datefin').Value := null;

    try
      ZQuery1.ExecSQL;
    except
      on E: Exception do
        showmessage(E.Message);
    end;
    listeProjets;
  end;
end;

procedure TForm1.nouvellecategorieExecute(Sender: TObject);
begin
  if form7.showmodal = 1 then
  begin
    Form1.ZQuery1.sql.Text := 'INSERT INTO categories(titre,qui) VALUES (:titre,:qui)';
    Form1.ZQuery1.Params.ParamByName('titre').Value := form7.Edit1.Text;
    Form1.ZQuery1.Params.ParamByName('qui').Value := moi;
    try
      ZQuery1.ExecSQL;
    except
      on E: Exception do
        showmessage(E.Message);
    end;
  end;
end;

procedure TForm1.nouvelletacheExecute(Sender: TObject);
var
  I: Integer;
begin
  if form2.showmodal = mrOk then
  begin
    Form1.ZQuery1.sql.Text := 'INSERT INTO taches(titre,description,projet,avancement,dateavancement,datedebut,datefin,datetermine,categorie,qui) VALUES (:titre,:description,:projet,:avancement,:dateavancement,:datedebut,:datefin,:datetermine,:categorie,:qui)';
    Form1.ZQuery1.Params.ParamByName('titre').Value := form2.Edit1.Text;
    Form1.ZQuery1.Params.ParamByName('description').Value := form2.memo1.Text;
    Form1.ZQuery1.Params.ParamByName('projet').Value := form2.combobox1.Text;
    Form1.ZQuery1.Params.ParamByName('avancement').Value := form2.ComboBox2.Text;
    Form1.ZQuery1.Params.ParamByName('dateavancement').Value := 'NOW()';
    Form1.ZQuery1.Params.ParamByName('categorie').Value := form2.combobox3.Text;
    Form1.ZQuery1.Params.ParamByName('qui').Value := moi;
    if form2.CheckBox1.checked then
      Form1.ZQuery1.Params.ParamByName('datedebut').Value := datetostr(form2.DateTimePicker1.DateTime)
    else
      Form1.ZQuery1.Params.ParamByName('datedebut').Value := null; // 'NULL';
    if form2.CheckBox2.checked then
      Form1.ZQuery1.Params.ParamByName('datefin').Value := datetostr(form2.DateTimePicker2.DateTime)
    else
      Form1.ZQuery1.Params.ParamByName('datefin').Value := null; // 'NULL';
    if form2.CheckBox3.checked then
      Form1.ZQuery1.Params.ParamByName('datetermine').Value := datetostr(form2.DateTimePicker3.DateTime)
    else
      Form1.ZQuery1.Params.ParamByName('datetermine').Value := null; // 'NULL';
    try
      ZQuery1.ExecSQL;
    except
      on E: Exception do
        showmessage(E.Message);
    end;

    for I := 0 to form2.Listbox1.Count - 1 do
    begin
      ZQuery1.sql.Text := 'INSERT INTO predeceseurs(tache,predeceseur) VALUES(:tache,:pred)';
      ZQuery1.ParamByName('tache').Value := form2.Edit1.Text;
      ZQuery1.ParamByName('pred').Value := form2.Listbox1.Items[I];
      try
        ZQuery1.ExecSQL;
      except
        on E: Exception do
          showmessage(E.Message);
      end;
    end;
    listeTaches;
  end;
end;

procedure TForm1.PaintBox1Paint(Sender: TObject);
begin
  PaintBox1.Canvas.draw(0, 0, buffer);
end;

procedure TForm1.PopupMenu1Popup(Sender: TObject);
begin
  if ListView2.Selected <> nil then
  begin
    Supprimerleprojet1.Caption := 'Supprimer le projet: ' + ListView2.Selected.Caption;
    Editerleprojet1.Caption := 'Editer le projet: ' + ListView2.Selected.Caption;
    ZQuery1.sql.Text := 'SELECT qui FROM projets WHERE titre=:titre';
    ZQuery1.ParamByName('titre').Value := ListView2.Selected.Caption;
    ZQuery1.Open;
    ZQuery1.First;
    Supprimerleprojet1.Enabled := ZQuery1.FieldByName('qui').AsString = Form1.moi;
    Editerleprojet1.Enabled := ZQuery1.FieldByName('qui').AsString = Form1.moi;
    exit;
  end
  else
  begin
    Supprimerleprojet1.Caption := 'Choisir un projet � supprimer';
    Editerleprojet1.Caption := 'Choisir un projet � �diter';
  end;
  Supprimerleprojet1.Enabled := (ListView2.Selected <> nil) and (ListView2.Selected.Data <> nil);
  Editerleprojet1.Enabled := (ListView2.Selected <> nil) and (ListView2.Selected.Data <> nil);
end;

procedure TForm1.PopupMenu2Popup(Sender: TObject);
begin
  (*
    if (ListView1.Selected <> nil) and (ListView1.Selected.Data <> nil) then
    begin
    Supprimerlatche1.Caption := 'Supprimer la t�che: ' + Tligne(ListView1.Selected.Data)['titre'];
    ZQuery1.sql.Text := 'SELECT qui FROM taches WHERE titre=:titre';
    ZQuery1.ParamByName('titre').Value := Tligne(ListView1.Selected.Data)['titre'];
    ZQuery1.Open;
    ZQuery1.First;
    Supprimerlatche1.Enabled := ZQuery1.FieldByName('qui').AsString = Form1.moi;
    exit;
    end
    else
    Supprimerlatche1.Caption := 'Choisir une t�che � supprimer';
    Supprimerlatche1.Enabled := (ListView1.Selected <> nil) and (ListView1.Selected.Data <> nil);
    *)
end;

procedure TForm1.PopupMenu3Popup(Sender: TObject);
begin
  if TreeView1.Selected.Data <> nil then
  begin
    erminer1.Caption := 'Terminer la t�che: ' + Tligne(TreeView1.Selected.Data)['titre'];
    ZQuery1.sql.Text := 'SELECT qui FROM taches WHERE titre=:titre';
    ZQuery1.ParamByName('titre').Value := Tligne(TreeView1.Selected.Data)['titre'];
    ZQuery1.Open;
    ZQuery1.First;
    erminer1.Enabled := ZQuery1.FieldByName('qui').AsString = Form1.moi;
    exit;
  end
  else
    erminer1.Caption := 'Choisir une t�che � terminer';
  erminer1.Enabled := (TreeView1.Selected <> nil) and (TreeView1.Selected.Data <> nil);
end;

procedure TForm1.quitterExecute(Sender: TObject);
begin
  Close;
end;

procedure TForm1.readSQL(const q: TZQuery);
var
  I, img: Integer;
  t: Tstringlist;
  cl: Tcolor;
begin
  metrogrid1.BeginUpdate;
  metrogrid1.clearCols;
  metrogrid1.Clear;

  for I := 0 to q.FieldDefs.Count - 1 do
    metrogrid1.addCol(q.FieldDefs[I].Name);

  q.First;
  while not q.Eof do
  begin
    if q.FieldByName('avancement').AsString = '100' then
    begin
      img := 497;
      cl := $76FFC1;
    end
    else if (not q.FieldByName('datefin').IsNull) and (q.FieldByName('datefin').AsDateTime < now()) then
    begin
      img := 238;
      cl := $769EFF;
    end
    else
    begin
      img := 521;
      cl := $FFC176;
    end;
    t := Tstringlist.Create;
    for I := 0 to q.Fields.Count - 1 do
      t.add(q.FieldDefs[I].Name + '=' + q.Fields[I].AsString);
    metrogrid1.addRowObject(t, Tligne.Create(ZQuery1.Fields, ZQuery1.FieldDefs), img, cl);
    // metrogrid1.addRow(t);
    // t.Free;
    q.Next;
  end;

  metrogrid1.EndUpdate;
  (*
    try
    ListView1.Columns.BeginUpdate;
    ListView1.Columns.Clear;
    for I := 0 to q.FieldDefs.Count - 1 do
    begin
    c := ListView1.Columns.add;
    c.Caption := q.FieldDefs[I].Name;
    c.Width := Length(q.FieldDefs[I].Name) * 9;
    end;
    finally
    ListView1.Columns.EndUpdate;
    end;

    try
    ListView1.Items.BeginUpdate;
    ListView1.Items.Clear;
    q.First;
    while not q.Eof do
    begin
    it := ListView1.Items.add;
    if q.FieldByName('avancement').AsString = '100' then
    it.ImageIndex := 497
    else if (not q.FieldByName('datefin').IsNull) and (q.FieldByName('datefin').AsDateTime < now()) then
    it.ImageIndex := 238
    else
    it.ImageIndex := 521;
    it.Data := Tligne.Create(ZQuery1.Fields, ZQuery1.FieldDefs);
    for I := 0 to q.Fields.Count - 1 do
    begin
    if Length(q.Fields[I].AsString) * 14 > ListView1.Columns[I].Width then
    ListView1.Columns[I].Width := Length(q.Fields[I].AsString) * 14;
    if I = 0 then
    begin
    it.Caption := q.Fields[0].AsString;
    end
    else if not q.Fields[I].IsNull then
    begin
    if (q.FieldDefs[I].Name = 'avancement') then
    it.SubItems.add(q.Fields[I].AsString + '%')
    else if (q.FieldDefs[I].Name = 'datefin') or (q.FieldDefs[I].Name = 'datedebut') or (q.FieldDefs[I].Name = 'datetermine') then
    it.SubItems.add(humandate(q.Fields[I].AsDateTime))
    else
    it.SubItems.add(q.Fields[I].AsString);
    end
    else
    it.SubItems.add('');
    end;
    q.Next;
    end;
    finally
    ListView1.Items.EndUpdate;
    end;
    *)
end;

function roundmmToPxScreen(const Value: single): Integer;
begin
  result := round(INCHPERMM * Value * screen.pixelsPerInch);
  // log(floattostr(value)+'mm = '+inttostr(result)+'px �cran');
end;

procedure TForm1.savecalExecute(Sender: TObject);
var
  b: Tbitmap;
  j: TJpegImage;
begin
  SaveDialog1.FileName := form9.Label2.Caption + ' au ' + formatDateTime('dd-mm-yyyy', now) + '.jpg';
  if SaveDialog1.Execute then
  begin
    setPrinterA4;
    printer.Orientation := poLandscape;
    b := Tbitmap.Create;
    b.PixelFormat := pf24bit;
    b.SetSize(roundmmToPxScreen(GetDeviceCaps(printer.Handle, HORZSIZE)), roundmmToPxScreen(GetDeviceCaps(printer.Handle, VERTSIZE)));
    form9.draw(b.Canvas, True);
    j := TJpegImage.Create;
    try
      j.Assign(b);
      j.SaveToFile(SaveDialog1.FileName);
    finally
      j.Free
    end;
    b.Free;
    form9.PaintBox1.Repaint;
  end;
end;

procedure TForm1.Splitter2Moved(Sender: TObject);
begin
  drawProjet;
end;

end.
