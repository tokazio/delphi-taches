object Form4: TForm4
  Left = 0
  Top = 0
  Caption = 'Nouveau commentaire'
  ClientHeight = 193
  ClientWidth = 632
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Segoe UI Light'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    632
    193)
  PixelsPerInch = 96
  TextHeight = 21
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 91
    Height = 21
    Caption = 'Commentaire'
  end
  object Memo1: TMemo
    Left = 8
    Top = 32
    Width = 617
    Height = 121
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssVertical
    TabOrder = 0
    OnChange = Memo1Change
  end
  object Button2: TButton
    Left = 456
    Top = 160
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Annuler'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button1: TButton
    Left = 548
    Top = 160
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Ok'
    TabOrder = 2
    OnClick = Button1Click
  end
end
