unit commentaireFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TmetroSeparatorU, Ttache2013U, TmetroCustomcontrolU,
  TmetroLabelU, TmetrobuttonU;

type
  TForm6 = class(TForm)
    Memo2: TMemo;
    Label6: TLabel;
    metroSeparator1: TmetroSeparator;
    metroLabel1: TmetroLabel;
    metroLabel2: TmetroLabel;
    metroButton1: TmetroButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure metroLabel1Click(Sender: TObject);
    procedure metroLabel2Click(Sender: TObject);
    procedure metroButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    tache: string;
    id: integer;
  end;

var
  Form6: TForm6;

implementation

uses nouveauCommentaireFormU, mainFormU, tacheformU;
{$R *.dfm}

procedure TForm6.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TForm6.metroButton1Click(Sender: TObject);
begin
  form4.new;
  if form4.showModal = 1 then
  begin
    form1.ZQuery1.SQL.Text := 'INSERT INTO commentaires(description,tache,parent,date,qui) VALUES(:desc,:tache,:id,NOW(),:qui)';
    form1.ZQuery1.Params.ParamByName('desc').Value := form4.memo1.Text;
    form1.ZQuery1.Params.ParamByName('tache').Value := tache;
    form1.ZQuery1.Params.ParamByName('id').Value := id;
    form1.ZQuery1.Params.ParamByName('qui').Value := form1.moi;
    try
      form1.ZQuery1.ExecSQL;
    except
      on E: Exception do
        showmessage(E.Message);
    end;
    form5.listecommentaires;
  end;
end;

procedure TForm6.metroLabel1Click(Sender: TObject);
var
  buttonSelected: integer;
begin
  buttonSelected := MessageDlg('Etes vous s�r de vouloir supprimer le commentaires et toutes ses r�ponses?', mtCustom, [mbYes, mbNo], 0);
  if buttonSelected = mrYes then
  begin
    form1.ZQuery1.SQL.Text := 'DELETE FROM commentaires WHERE id=' + inttostr(id) + ' OR parent=' + inttostr(id);
    try
      form1.ZQuery1.ExecSQL;
    except
      on E: Exception do
        showmessage(E.Message);
    end;
    form5.listecommentaires;
  end;
end;

procedure TForm6.metroLabel2Click(Sender: TObject);
begin
  form4.Memo1.Lines.Assign(memo2.Lines);
  if form4.showModal = 1 then
  begin
    form1.ZQuery1.SQL.Text := 'UPDATE commentaires SET description=:desc,datemodif=NOW() WHERE id='+inttostr(id);
    form1.ZQuery1.Params.ParamByName('desc').Value :=form4.Memo1.Text;
    try
      form1.ZQuery1.ExecSQL;
    except
      on E: Exception do
        showmessage(E.Message);
    end;
  end;
  form5.listeCommentaires;
end;

end.
