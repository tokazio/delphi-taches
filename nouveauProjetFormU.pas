unit nouveauProjetFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, TmetroSeparatorU, ExtCtrls, TmetroCustomcontrolU,
  TmetrobuttonU, TmetroLabelU, TmetroFormU;

type
  TForm3 = class(TmetroForm)
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Memo1: TMemo;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    Label3: TLabel;
    ComboBox1: TComboBox;
    Label4: TLabel;
    Panel1: TPanel;
    metroSeparator3: TmetroSeparator;
    metroButton2: TmetroButton;
    metroLabel1: TmetroLabel;
    Image1: TImage;
    procedure Edit1Change(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure metroButton2Click(Sender: TObject);
    procedure metroLabel1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure check;
    procedure listeCategories;
    { Private declarations }
  public
    { Public declarations }
    ancienTitre:String;
    procedure clear;
  end;

var
  Form3: TForm3;

implementation

uses mainFormU;

{$R *.dfm}

procedure TForm3.Edit1Change(Sender: TObject);
begin
  check;
end;

procedure TForm3.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
  begin
    key:=#0;
    metrobutton2click(sender);
  end;

end;

procedure TForm3.FormCreate(Sender: TObject);
begin
  metroSeparator3.color := $00D4D4D4;
end;

procedure TForm3.FormShow(Sender: TObject);
begin
  clear;
  listeCategories;
end;

procedure Tform3.check;
begin
  metrobutton2.Enabled:=edit1.Text<>'';
end;

procedure Tform3.listeCategories;
begin
  try
    ComboBox1.Items.BeginUpdate;
    ComboBox1.Clear;
    form1.ZQuery1.SQL.Text := 'SELECT titre FROM categories ORDER BY titre';
    form1.ZQuery1.Open;
    form1.ZQuery1.first;
    while not form1.ZQuery1.Eof do
    begin
      ComboBox1.Items.Add(form1.ZQuery1.FieldByName('titre').AsString);
      form1.ZQuery1.Next;
    end;
  finally
    form1.ZQuery1.close;
    ComboBox1.Items.EndUpdate;
  end;
end;

procedure TForm3.metroButton2Click(Sender: TObject);
begin
  modalResult:=1;
end;

procedure TForm3.metroLabel1Click(Sender: TObject);
begin
  close;
end;

procedure TForm3.CheckBox1Click(Sender: TObject);
begin
  DateTimePicker1.Enabled := CheckBox1.Checked;
end;

procedure TForm3.CheckBox2Click(Sender: TObject);
begin
  DateTimePicker2.Enabled := CheckBox2.Checked;
end;

procedure Tform3.clear;
begin
  memo1.Clear;
  edit1.Clear;
  datetimePicker1.DateTime:=now();
  datetimePicker2.DateTime:=now();
  metrobutton2.Enabled:=false;
  CheckBox1.checked := false;
  CheckBox2.checked := false;

  DateTimePicker1.Enabled := false;
  DateTimePicker2.Enabled := false;
  listecategories;

  label4.Caption:='Aujourd''hui le '+dateToStr(now);
end;

end.
